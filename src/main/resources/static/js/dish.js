addToCardBtn.addEventListener("click", function () {

var addToCardBtn = document.getElementById("addToCardBtn");
var quantity = 1;

const queryString = window.location.search;
console.log(queryString);

const urlParams = new URLSearchParams(queryString);

const hour = urlParams.get('hour')
console.log(hour);

const minute = urlParams.get("minute");
console.log(minute);

const date = urlParams.get("date");
console.log(date);

var path = window.location.pathname.split('/');
var dishId = path[path.length-1];
console.log("dish id:" + dishId);

var price = parseFloat(document.getElementById("dishPrice").innerHTML.split(" ")[0]);
console.log("price:" + price);

// default behaviour
var delivery = true;

    quantity = parseInt(document.getElementById("dishQuantitySelect").value);
    console.log(quantity);

    var item = {
        dishId: dishId,
        price: price,
        date: date,
        quantity: quantity,
        hour: hour,
        minute: minute,
        delivery: delivery
    };

//    var cookie = getCookie("items", JSON.stringify(item), 1000);

    // there is only one cookie so we can do this. If we add another cookie
    // we MUST change this code(the session cookie isn't accessible with document.cookie
    // so it doesn't count
    var previousCookies = document.cookie;
    console.log("previousCookies:" + previousCookies);
    console.log("previousCookies length:" + previousCookies.length);
    if(previousCookies == null)
        console.log("previousCookies is null");
    else if(previousCookies == undefined)
        console.log("previousCookies is null");

    var items;

    // remove item with same dishId if it exists
    if(previousCookies.length > 0) {
    items = previousCookies.split(";")[0].split("=")[1];
    items = JSON.parse(unescape(items));
    console.log("items:" + items);
    items = items.filter(currentItem => currentItem.dishId != item.dishId);
    } else {
    items = [];
    }
    items.push(item);

    var cookies = getCookie("items", escape(JSON.stringify(items)), 1000);
    console.log("cookies:" + cookies);
    document.cookie = cookies;
    console.log("document cookies:" + document.cookie);

    refreshCartTotal()


})

var rateButton = document.getElementById("rateButton")

rateButton.addEventListener("click", function(event) {
    var target = event.target
    var stars = document.getElementById("starsSelect").value
    console.log("star:" + stars)

    let fd = new FormData();
    fd.append("stars", stars);

    var dishId = document.getElementById("dishId").innerHTML

    console.log("dish id:" + dishId)
    fd.append("dishId", dishId);

    fetch("rateDish", {
        method: 'post',
        body: fd
    }).then(function(response) {
           console.log(response.text());
    })
})







