
function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
};


    var locationForm = document.getElementById("locationForm");


    locationForm.addEventListener("submit", function(e) {

    e.preventDefault();

      if (navigator.geolocation) {
             navigator.geolocation.getCurrentPosition(showPosition, showError);
      }

    });

    console.log("After");




function showPosition(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;

  console.log("latitude:" + latitude);
  console.log("longitude:" + longitude);

    //TODO : change in production with the url of the server
  fetch("api/location", {
  method: 'post',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({latitude : latitude, longitude: longitude})
}).then(function(response) {
     return response.text();
}).then(function (text) {
      console.log(text + " od serverot");
}).catch(function (error)  {
  console.log(error);
});

}

function showError(error, error2) {
  window.localStorage.setItem("geolocation", "error");
  switch(error.code) {
    case error.PERMISSION_DENIED:
      alert("Enable location on your browser");
      break;
  }
}
