function refreshCartTotal() {
    console.log("refreshCartTotal called");
    var itemPosition = document.cookie.indexOf('items');
    console.log("items exist?:" + itemPosition);

    // this means that the cookie exists
    if(itemPosition >= 0) {

        var cartNavTotal = document.getElementById("cartNavTotal");

        var total = sumOfDishPrices(document.cookie);
        total = Number(total).toFixed(2);
        total = parseFloat(total);
        document.getElementById("cartNavTotal").innerHTML = total;
        return total;
    }
    return 0;
}

refreshCartTotal();
