function getCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  return cname + "=" + cvalue + "; " + expires + "; path=/" + "; sameSite=Lax; ";
}

function sumOfDishPrices(cookies) {
    items = cookies.split(";")[0].split("=")[1];
    items = JSON.parse(unescape(items));
    console.log("sum items:" + items);
    console.log("sum itemslength:" + items.length);
    var sum = 0;

    for(var i = 0; i < items.length; i++) {
        sum += (items[i].quantity * items[i].price);
    }

    return sum;
}