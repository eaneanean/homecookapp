var cardNumberInput = document.getElementById("cardNumberInput");

cardNumberInput.addEventListener("input", function (e) {

    var creditCardNumber =  e.target.value;

    document.getElementById("nextBtnCard").disabled = true;

    if(creditCardNumber.length == 16) {
        if(!luhn(creditCardNumber)) {
        document.getElementById("cardNumberError").innerHTML = "Credit card number is invalid"
        }
        else {
         document.getElementById("cardNumberError").innerHTML = ""
         document.getElementById("nextBtnCard").disabled = false;
        }
    }

})

function luhn (cardNumber) {

    console.log("luhn")
    var oddSum = 0

    for(var i = 15; i >= 0; i = i - 2) {
        console.log("i:" + i)
        oddSum += parseInt(cardNumber.charAt(i));
    }

    var evenSum = 0;

    for(var i = 14; i >= 0; i = i -2) {
        var even = parseInt(cardNumber.charAt(i));
        var evenTimesTwo = even * 2;

        if(evenTimesTwo < 10)
            evenSum += evenTimesTwo;
        else {
            var sumOfChar = parseInt(evenTimesTwo.toString().charAt(0)) +
            parseInt(evenTimesTwo.toString().charAt(1));
            evenSum += sumOfChar;
        }
    }

    var totalSum = oddSum + evenSum;

    console.log("sum" + totalSum)

    return totalSum % 10 == 0;

}