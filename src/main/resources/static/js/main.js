window.onload =  function getLocation() {
         if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(showPosition, showError);
  }

  locationForm();

};

function locationForm() {

    var locationForm = document.getElementById("locationForm");

    locationForm.addEventListener("submit", function(e) {

        console.log("locationForm()")
        e.preventDefault();

        console.log(this);
        const formData = new FormData(this);

        fetch("api/location", {
            method: 'post',
            body: formData
        }).then(function(response) {
               console.log(response.text());
        })

    });
}

function showPosition(position) {
  document.getElementById("latitude").value = position.coords.latitude;
  document.getElementById("longitude").value = position.coords.longitude;
}

function showError(error, error2) {
  window.localStorage.setItem("geolocation", "error");
  switch(error.code) {
    case error.PERMISSION_DENIED:
      alert(map["javascript.userDeniedAccess"]);
      break;
  }
}
