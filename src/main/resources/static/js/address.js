var deleteAddressButtons = document.getElementsByClassName("deleteAddressButton")

function getTableRowElement(event) {
    var clickedButton = event.target
    var td = clickedButton.parentElement
    var tr = td.parentElement
    return tr;
}

function getAddressIdFromButton(event) {
    var tr = getTableRowElement(event)
    var addressIds = tr.getElementsByClassName("myAddressId")[0];
    return addressIds.innerHTML
}

function removeAddressRow(event) {
    var tr = getTableRowElement(event)
    tr.parentNode.removeChild(tr);
}

function deleteAddressRowFromDb(addressId) {
    let fd = new FormData();
    fd.append("addressId", addressId);

    fetch("myaddress", {
        method: 'delete',
        body: fd
    }).then(function(response) {
           console.log(response.text());
    })
}



Array.from(deleteAddressButtons).
    forEach((e) => {
        e.addEventListener("click", function (event) {
            var addressId = getAddressIdFromButton(event)
            console.log("Address id:" + addressId)
            removeAddressRow(event)
            deleteAddressRowFromDb(addressId)
        })
    })


