var cartItems = document.getElementsByClassName("cartItem")

console.log("cart.js");

for(var i = 0; i < cartItems.length; i++) {
    var cartItem = cartItems[i];
    var price = parseFloat(cartItem.getElementsByClassName("price")[0].innerHTML)
    price = Number(price).toFixed(2);
    price = parseFloat(price);
    console.log("price:" + price);

    var dishId = cartItem.getElementsByClassName("dishId")[0].value
    console.log("dish id:" + dishId)

    var quantityElement = cartItem.getElementsByClassName("quantitySelect")[0]

    quantityElement.addEventListener("change", quantityChanged);

    var isDeliveryChosen = cartItem.getElementsByClassName("isDeliveryChosen")[0];

    if(isDeliveryChosen.checked)
        console.log("delivery chosen")
    else
        console.log("delivery not chosne")

    var removeItemButton = cartItem.getElementsByClassName("removeItem")[0]
    removeItemButton.addEventListener("click", removeItemButtonClicked)

    isDeliveryChosen.addEventListener("change", deliveryChanged)

    var quantity = quantityElement.value;

    console.log("quantity:" + quantity);
    var total = quantity * price
     total = Number(total).toFixed(2);
     total = parseFloat(total);

    console.log("total:" + total);
    cartItem.getElementsByClassName("total")[0].textContent = total;
}

function deliveryChanged(event) {
    var deliveryInput = event.target;
    var deliveryValue = false;
    if(deliveryInput.checked)
        deliveryValue = true;

    var clickedCartItem = deliveryInput.parentElement.parentElement;

    var dishId = clickedCartItem.getElementsByClassName("dishId")[0].value
    var itemsArray = getItemsFromCookies();

    for(var i = 0; i < itemsArray.length; i++) {
        var item = itemsArray[i];
        if(item.dishId == dishId) {
            itemsArray[i].delivery = deliveryValue;
        }
    }

    document.cookie = getCookie("items", escape(JSON.stringify(itemsArray)), 1000);
}

function removeItemButtonClicked(event) {

    var buttonClicked = event.target
    var clickedCartItem = buttonClicked.parentElement.parentElement

    var dishId = clickedCartItem.getElementsByClassName("dishId")[0].value
    var items = getItemsFromCookies();
    items = items.filter(currentItem => currentItem.dishId != dishId);

    document.cookie = getCookie("items", escape(JSON.stringify(items)), 1000);

    clickedCartItem.remove()

    refreshCartTotal()

}

function quantityChanged(event) {

     var selected = event.target;
     var selectedValue = selected.value;

     var clickedCartItem = selected.parentElement.parentElement;

     var dishId = clickedCartItem.getElementsByClassName("dishId")[0].value
     console.log("dishId from selected:" + dishId);

     var itemsArray = getItemsFromCookies();

     for(var i = 0; i < itemsArray.length; i++) {
         var item = itemsArray[i];
         if(item.dishId == dishId) {
             itemsArray[i].quantity = selectedValue;
         }
     }

     document.cookie = getCookie("items", escape(JSON.stringify(itemsArray)), 1000);

     var price = parseFloat(clickedCartItem.getElementsByClassName("price")[0].innerHTML)
     price = Number(price).toFixed(2);
     price = parseFloat(price);

     var total = selectedValue * price;
     total = Number(total).toFixed(2);
     total = parseFloat(total);

     var totalElement = clickedCartItem.getElementsByClassName("total")[0];
     totalElement.innerHTML = total;

     refreshCartTotal()
}

function getItemsFromCookies() {
    var cookies = document.cookie;
    var items = cookies.split(";")[0].split("=")[1];
    items = JSON.parse(unescape(items));
    return items;
}