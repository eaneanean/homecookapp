
let searchButton = document.getElementById("searchButton")

function getSearchWordAndGoThePage() {
    let url = document.getElementById("urlForSearch").innerText
    let searchWord = document.getElementById("searchWord").value
    let completeUrl = url + "&searchWord=" + searchWord
    window.location.href = completeUrl
}

searchButton.addEventListener("click", function (event) {
    getSearchWordAndGoThePage();
})


document.getElementById("searchWord").addEventListener("keypress", function(event) {

    if(event.keyCode == 13)
        getSearchWordAndGoThePage();

})