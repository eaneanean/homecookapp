package cf.homecook.validator;

import cf.homecook.dto.TimeOffForm;
import cf.homecook.entity.TimeOff;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Slf4j
public class TimeOffFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return TimeOffForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        TimeOffForm timeOffForm = (TimeOffForm) target;

        TimeOff timeOff = timeOffForm.toTimeOff(new TimeOff());

        int compare = timeOff.getFromTime().compareTo(timeOff.getUntilTime());

        log.debug("Compare value:" + compare);

        if(compare >= 0)
            errors.rejectValue("untilMinute", "error.mustBeBigger");

    }
}
