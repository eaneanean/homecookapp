package cf.homecook.validator;

import cf.homecook.dto.AddDishForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AddDishFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return AddDishForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        AddDishForm dishForm = (AddDishForm) target;

        if(dishForm.getPriceInEuro() < 0.5 ||
            dishForm.getPriceInEuro() > 200) {
            errors.rejectValue("priceInEuro", "error.price");

        }

    }
}
