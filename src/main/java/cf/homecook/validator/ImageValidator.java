package cf.homecook.validator;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Arrays;

@Component
@Slf4j
public class ImageValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return MultipartFile.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        MultipartFile image = (MultipartFile) o;

        String type = "";

        try {
            byte[] bytes = image.getBytes();

            ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
            type = URLConnection.guessContentTypeFromStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            errors.rejectValue("imageFile","error.imageFormat");
        }

        boolean typeIsValid = false;

        if(type == null)
            typeIsValid = false;
        else if(type.equals("image/png") || type.equals("image/jpeg") ||  type.equals("image/gif")
                || type.equals("image.jpg")) {
            typeIsValid = true;
        }


        if(!typeIsValid) {
            errors.rejectValue("imageFile", "error.imageFormat");
            log.debug("File type is:" + type);
        }

    }
}