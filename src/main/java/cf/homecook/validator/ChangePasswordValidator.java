package cf.homecook.validator;

import cf.homecook.dto.ChangePasswordForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ChangePasswordValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return ChangePasswordForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ChangePasswordForm form = (ChangePasswordForm) o;

        if(!form.getPassword().equals(form.getRepeatPassword())) {
            errors.rejectValue("repeatPassword", "error.differentPasswords");
        }

        if(form.getPassword().length() < 8) {
            errors.rejectValue("password", "error.register.pass");
        }




    }
}
