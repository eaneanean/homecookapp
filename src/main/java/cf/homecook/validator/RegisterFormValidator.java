package cf.homecook.validator;

import cf.homecook.dto.RegisterForm;
import cf.homecook.entity.User;
import cf.homecook.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Slf4j
public class RegisterFormValidator implements Validator {

    @Autowired
    UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return RegisterForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        // we can be sure of this because of the check that the supports
        // method does before calling this method
        RegisterForm registerForm = (RegisterForm) target;

        if (!registerForm.getUnencryptedPassword().equals(registerForm.getUnencryptedRepeatedPassword())) {
            errors.rejectValue("unencryptedRepeatedPassword", "error.differentPasswords");
        }

        if(registerForm.getUnencryptedPassword().length() < 8) {
            errors.rejectValue("unencryptedPassword", "error.shortPassword");
        }

        if(registerForm.getEmail() == null || registerForm.getEmail().isEmpty()) {
            errors.rejectValue("email", "error.emptyEmail");
        }

        if(userService.isEmailUsed(registerForm.getEmail()))
            errors.rejectValue("email", "error.usedEmail");

        if(!registerForm.getRole().equals(User.ROLE_CLIENT) &&
        !(registerForm.getRole()).equals(User.ROLE_COOK)) {

            log.warn("User is trying to temper with the html form." +
                    " As value used:" + registerForm.getRole());
            registerForm.setRole(User.ROLE_CLIENT);
        }

    }
}
