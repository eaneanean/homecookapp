package cf.homecook.validator;

import cf.homecook.dto.ForgetForm;
import cf.homecook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Locale;

@Component
public class ForgetFormValidator implements Validator {

    @Autowired
    UserService userService;


    @Override
    public boolean supports(Class<?> aClass) {
        return ForgetForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ForgetForm forgetForm = (ForgetForm) o;

        if (!userService.isEmailUsed(forgetForm.getEmail())) {
            errors.rejectValue("email", "notRegisteredEmail.error");
        }

    }

}