package cf.homecook.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.TimeZone;

@Configuration
@Slf4j
public class TimeZoneConfiguration {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void init(){

        // Setting Spring Boot SetTimeZone
        log.debug("Timezone");

        LocalDate today = LocalDate.now();
        log.debug("Local time:" + today + " " + LocalTime.now());

        String date = jdbcTemplate.queryForObject("SELECT NOW();", String.class);
        log.debug("Time and date from sql:" + date);
    }


}
