package cf.homecook.service;

import org.springframework.stereotype.Service;

@Service
public interface DayService {

    public String dayToDayName(int dayNumber);
}
