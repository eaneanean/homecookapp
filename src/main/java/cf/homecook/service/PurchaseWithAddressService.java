package cf.homecook.service;

import cf.homecook.dto.PurchaseWithAddress;
import org.springframework.stereotype.Service;

@Service
public interface PurchaseWithAddressService {

    public void findAndPutAddress(PurchaseWithAddress purchaseWithAddress);
}
