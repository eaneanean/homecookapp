package cf.homecook.service;

import cf.homecook.dto.AddToCard;
import cf.homecook.entity.Dish;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import cf.homecook.repo.DishRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static cf.homecook.utils.Common.addZero;

@Service
@Slf4j
public class CartServiceImp implements CartService {

    String itemsJSON = "";

    @Autowired
    DishRepository dishRepository;

    public List<AddToCard> cookiesToAddToCard(Cookie[] cookies) {
        log.debug("Cookie content:");
        Arrays.stream(cookies).forEach(cookie -> log.debug(cookie.getName() + " " + cookie.getValue()));
        if (cookies != null) {
            itemsJSON = Arrays.stream(cookies)
                    .filter(c -> c.getName().equals("items"))
                    .map(c -> {
                        try {
                            return c.getName() + "=" + URLDecoder.decode(c.getValue(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        return null;
                    })
                    .collect(Collectors.joining());
            log.debug("cookies value:" + itemsJSON);
            log.debug("cookies size:" + cookies.length);
            log.debug("itemsJSON:" + itemsJSON);
        }
        else {
            log.debug("cookies not valid");
        }

        // get the value in key=value pair
        itemsJSON = itemsJSON.split("=")[1];
        log.debug("itemsJSON" + itemsJSON);

        ObjectMapper objectMapper = new ObjectMapper();
        List<AddToCard> listCar = new ArrayList<>();
        try {
            listCar = objectMapper.readValue(itemsJSON, new TypeReference<List<AddToCard>>(){});
            listCar.stream().forEach(item -> log.debug(item.toString()));
        } catch (IOException e) {
            log.error(e.toString());
        }
        return listCar;

    }


    /**
     * Checks whether the quantity of the user's shopping cart are still available
     * @param addToCardList
     * @return null if there is no error. The error message, if there is one. Because
     * there can be more than one error, the error message are sepereted by \n
     */
    public String checkQuantity(List<AddToCard> addToCardList, HttpServletResponse response) {

        String errorMessages = "";

        boolean isThereAnyError = false;
        List<AddToCard> addToCardListChangedWithError = new ArrayList<>();

        for(AddToCard addToCard : addToCardList) {

            boolean isThereErrorInThisDish = false;

            Optional<Dish> currentDishOptional = dishRepository.findById(addToCard.getDishId());
            if(currentDishOptional.isPresent()) {
                Dish currentDish = currentDishOptional.get();
                if(currentDish.getQuantity() < addToCard.getQuantity()) {
                    String errorMessage = "The max available quantity for " + currentDish.getTitle() + " is " +
                            currentDish.getQuantity() + ". You have chosen " + addToCard.getQuantity();
                    log.debug(errorMessage);
                    errorMessages += errorMessage + "<br />";
                    isThereAnyError = true;
                    isThereErrorInThisDish = true;
                }
                else if(addToCard.getQuantity() == 0) {
                    if(currentDish.getQuantity() == 0)
                        errorMessages += currentDish.getTitle() + " is not available right now" + "<br />";
                    else
                        errorMessages += currentDish.getTitle() + " was not available, but now is" + "<br />";
                }

                LocalDateTime now  = LocalDateTime.now();
                LocalDateTime nowPlusTimeToPrepare = now.plusMinutes(currentDish.getTimeToPrepareInMinutes());

                String dishChosenTimeLocalTimeFormat =
                        addToCard.getDate() + "T" + addZero(addToCard.getHour()) + ":"
                        + addZero(addToCard.getMinute()) + ":00";

                LocalDateTime dishChosenTime = LocalDateTime.parse(dishChosenTimeLocalTimeFormat);

                log.debug("dishChosenTime" + dishChosenTime);
                log.debug("nowPlusTimeToPrepare" + nowPlusTimeToPrepare);

                if(nowPlusTimeToPrepare.isAfter(dishChosenTime)) {
                    isThereAnyError = true;
                    isThereErrorInThisDish = true;
                    log.debug("Not enought time to prepare for " + currentDish.getTitle());
                    errorMessages += "Not enought time to prepare for " + currentDish.getTitle() + "<br />";
                }

                if(isThereErrorInThisDish) {
                    addToCard.setQuantity(0);
                    addToCardListChangedWithError.add(addToCard);
                }
                else {
                    addToCardListChangedWithError.add(addToCard);
                }

            }
            else {
                log.debug("Dish with that id is not found. This could happen " +
                        "if the cook deleted the dish since the user added it to the" +
                        " card or when the user tempered with the cookie");
            }

        }

        if(isThereAnyError) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String json = objectMapper.writeValueAsString(addToCardListChangedWithError);
                Cookie cookie = new Cookie("items", URLEncoder.encode(json, "UTF-8"));
                response.addCookie(cookie);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        return errorMessages.equals("") ? null : errorMessages;
    }


}
