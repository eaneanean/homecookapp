package cf.homecook.service;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

@Service
public class ConformationServiceImp implements ConformationService {

    @Autowired
    private EmailService emailService;

    @Autowired
    MessageSource messageSource;


    public void sendConformationMail(User user, Token token, String url) {
        emailService.sendSimpleMessage(user.getEmail(),
                messageSource.getMessage("email.conformation.title", null, Locale.ENGLISH),
                messageSource.getMessage("email.conformation", null, Locale.ENGLISH)
                        + url + "/" + token.getRandomString());
    }

    public void sendChangePassword(String email, Token token, String url) {
        emailService.sendSimpleMessage(email,
                messageSource.getMessage("email.password.title", null, Locale.ENGLISH),
                messageSource.getMessage("email.password", null, Locale.ENGLISH) + url + "/" + token.getRandomString());
    }

    public String getURI(HttpServletRequest request, String path) throws MalformedURLException, URISyntaxException {
        URL url = new URL(request.getRequestURL().toString());
        String host  = url.getHost();
        String userInfo = url.getUserInfo();
        String scheme = url.getProtocol();
        int port = url.getPort();
        URI uri = new URI(scheme,userInfo,host,port,path,null,null);
        return uri.toString();
    }
}
