package cf.homecook.service;

import cf.homecook.dto.AddToCard;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public interface CartService {
    public List<AddToCard> cookiesToAddToCard(Cookie[] cookies);

    public String checkQuantity(List<AddToCard> addToCardList, HttpServletResponse response);
}