package cf.homecook.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;

@Service
@Slf4j
public class ImageServiceDevImp implements ImageService {



    @Value("${imagePath}")
    String path;

    @Value("${smallImages.height}")
    String smallImagesHeight;

    public boolean saveOriginalImage(MultipartFile image, String imageName) throws IOException {

        log.debug(image.getContentType());
        byte[] bytes = image.getBytes();

        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage bImageFromConvert = ImageIO.read(in);

        log.debug("Path:" + path);

        return ImageIO.write(bImageFromConvert,
                "jpg", new File(path + imageName + ".jpg"));
    }

    public void saveImage(MultipartFile image, String imageName) throws IOException {
        saveOriginalImage(image, imageName);
        saveSmallImage(image, imageName);
    }

    public void saveSmallImage(MultipartFile image, String imageName) throws IOException {
        byte[] bytes = image.getBytes();

        InputStream in = new ByteArrayInputStream(bytes);
        BufferedImage bImageFromConvert = ImageIO.read(in);
        saveSmallBufferedImage(bImageFromConvert, imageName);
    }

    private void saveSmallBufferedImage(BufferedImage img, String imageName) throws IOException  {

        Image smallImage = img.getScaledInstance(-1, Integer.valueOf(smallImagesHeight), Image.SCALE_DEFAULT);
        BufferedImage bufferedImage = new BufferedImage(smallImage.getWidth(null), smallImage.getHeight(null), BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().drawImage(smallImage, 0, 0 , null);

        try {
            ImageIO.write(bufferedImage, "jpg", new File(path + ImageServiceHelper.PREFIX_SMALL_PICTURES +
                    imageName + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        bufferedImage.getGraphics().dispose();
    }

    public byte [] readImage(String imageName) throws IOException {

        File file = new File(path + imageName + ".jpg");
        InputStream input = new FileInputStream(file);
        byte [] bytes = input.readAllBytes();;

        input.close();

        return bytes;
    }

    public String readBase64OfImage(String imageName) throws IOException {
        return Base64.getEncoder().encodeToString(readImage(imageName));
    }



}

