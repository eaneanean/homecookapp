package cf.homecook.service;

import cf.homecook.dto.AddToCard;
import cf.homecook.dto.PurchaseWithAddress;
import cf.homecook.entity.Address;
import cf.homecook.entity.Dish;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import cf.homecook.repo.AddressRepository;
import cf.homecook.repo.DishRepository;
import cf.homecook.repo.OrderDishRepository;
import cf.homecook.repo.PurchaseRepository;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cf.homecook.utils.Common.addZero;

@Service
@Slf4j
public class CardServiceImp implements CardService {

    @Autowired
    DishRepository dishRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    OrderDishRepository orderDishRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    PurchaseWithAddressService purchaseWithAddressService;


    /**
     *
     * @param orderDish non null, empty OrderDish object that will be changed in this
     *                  method. If the id of the dish given in the addToCard is incorrect
     *                  the method will return orderDish without any change.
     * @param addToCard
     * @param purchase
     * @return
     */
    public OrderDish orderDishFromAddToCard(OrderDish orderDish, AddToCard addToCard, Purchase purchase) {


        Optional<Dish> optionalDish = dishRepository.findById(addToCard.getDishId());

        // if dish is not present user tempered with the
        // dish id
        if(optionalDish.isPresent()) {

            String fromDate = addToCard.getDate();
            int fromHour = addToCard.getHour();
            int fromMinute = addToCard.getMinute();
            String from = fromDate + " " + addZero(fromHour) + ":" + addZero(fromMinute) + ":" + "00";
            Dish dish = optionalDish.get();
            orderDish.setDish(dish);
            orderDish.setDelivery(addToCard.getDelivery());
            orderDish.setChosenTime(Timestamp.valueOf(from));
            orderDish.setPricePerDishInEuros(dish.getPriceInEuro());
            orderDish.setQuantity(addToCard.getQuantity());
            orderDish.setPurchase(purchase);
        }

        return orderDish;
    }

    /**
     *
     * @param addToCardList
     * @param savedPurchase changed with items that belong to this purchase and the total
     *                      amount of the purchase
     */
    @Override
    public void makePurchase(List<AddToCard> addToCardList, Purchase savedPurchase) {


        List<OrderDish> orderDishList = addToCardList
                .stream()
                .map(addToCard -> orderDishFromAddToCard(new OrderDish(),
                        addToCard, savedPurchase))
                .collect(Collectors.toList());

        double sum =
                orderDishList
                        .stream()
                        .map(addToCard -> addToCard.getQuantity() * addToCard.getPricePerDishInEuros())
                        .reduce(0.0, Double::sum);


        orderDishRepository.saveAll(orderDishList);
        savedPurchase.setItems(orderDishList);
        savedPurchase.setTotalInEuros(sum);
        purchaseRepository.save(savedPurchase);

        orderDishList
                .stream()
                .forEach(orderDish -> {
                    Optional<Dish> optionalDish = dishRepository.findById(orderDish.getDish().getId());

                    if(optionalDish.isEmpty())
                        throw new RuntimeException("Can't order non-existing dish");

                    Dish dish = optionalDish.get();

                    if(dish.getQuantity() < orderDish.getQuantity())
                        throw new RuntimeException("Can't order more dishes that are possible");


                    dish.setQuantity(dish.getQuantity() - orderDish.getQuantity());
                    dishRepository.save(dish);
                });



    }

    @Transactional
    @Override
    public PurchaseWithAddress makePayment(Purchase purchase, List<AddToCard> addToCardList) {
        // now it has an id
        Purchase savedPurchase = purchaseRepository.save(purchase);

        makePurchase(addToCardList, savedPurchase);
        // must sent emails: one to the user, and one mail for each cook
        PurchaseWithAddress purchaseWithAddress = new PurchaseWithAddress();
        purchaseWithAddress.setPurchase(purchase);
        purchaseWithAddressService.findAndPutAddress(purchaseWithAddress);
        List<OrderDish> orderDishList = purchaseWithAddress.getPurchase().getItems();
        for(int i = 0; i < orderDishList.size(); i++) {
            log.debug("Order dish card:" + orderDishList.get(i).toString());
            log.debug(purchaseWithAddress.getAddressForDishes().get(i).toString());
        }


        return purchaseWithAddress;
    }
}
