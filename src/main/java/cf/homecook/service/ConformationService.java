package cf.homecook.service;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

@Service
public interface ConformationService {

    public void sendConformationMail(User user, Token token, String url);
    public void sendChangePassword(String email, Token token, String url);
    public String getURI(HttpServletRequest request, String path) throws MalformedURLException, URISyntaxException;
}
