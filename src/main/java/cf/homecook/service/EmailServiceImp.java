package cf.homecook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImp implements EmailService {


    @Autowired
    public JavaMailSender emailSender;

    @Override
    public void sendSimpleMessage(String sendTo, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(sendTo);
        message.setSubject(subject);
        message.setText(text);

        emailSender.send(message);
    }

    public void sendPurchaseToClient() {

    }

}
