package cf.homecook.service;

import cf.homecook.dto.AddDishForm;
import cf.homecook.dto.ChangeDishForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import org.springframework.stereotype.Service;

@Service
public interface DishService {
    public Dish saveDish(AddDishForm addDishForm, Cook cook);
    public ChangeDishForm fromDish(ChangeDishForm changeDishForm, Dish dish);
    public Dish replaceDish(AddDishForm addDishForm, Cook cook, long dishId);
}
