package cf.homecook.service;

public class ImageServiceHelper {

    /**
     * The form of the picture is, for example if it is a dish with id 7:
     * dish_7, if it is a small dish small_dish_7
     * If there isn't a prefix we are talking about a profile picture of a cook:
     * For example cook with user id 7, will have picture 7, and small picture for the
     * same cook will be small_7
     */

    public static final String PREFIX_SMALL_PICTURES = "small_";
    public static final String PREFIX_DISH = "dish_";
    public static final String PREFIX_COOK = "cook_";
}
