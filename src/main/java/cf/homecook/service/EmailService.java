package cf.homecook.service;

import org.springframework.stereotype.Service;

@Service
public interface EmailService {
    void sendSimpleMessage(String sendTo, String subject, String text);
}
