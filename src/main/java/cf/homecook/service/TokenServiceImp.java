package cf.homecook.service;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import cf.homecook.repo.TokenRepository;
import cf.homecook.utils.RandomGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
@Slf4j
public class TokenServiceImp implements TokenService {

    @Autowired
    TokenRepository tokenRepository;

    @Override
    public Token createToken(User user) {
        Token token = new Token();
        token.setCreatedAt(Instant.now().toEpochMilli());
        token.setUser(user);


        try {
            token.setRandomString(RandomGenerator.generateRandomBytes(60));
        } catch (Exception e) {
            log.error(e.toString());
        }
        return token;
    }



    @Override
    public int saveToken(Token token) {
         Token savedToken =  tokenRepository.save(token);
         return (int) savedToken.getId();
    }

    @Override
    public Optional<Token> findTokenByTokenText(String token) {
        return tokenRepository.findByRandomString(token);
    }

}
