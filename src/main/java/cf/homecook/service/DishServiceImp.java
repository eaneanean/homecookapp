package cf.homecook.service;

import cf.homecook.dto.AddDishForm;
import cf.homecook.dto.ChangeDishForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import cf.homecook.entity.DishRating;
import cf.homecook.repo.DishRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static cf.homecook.service.ImageServiceHelper.PREFIX_DISH;
import static cf.homecook.service.ImageServiceHelper.PREFIX_SMALL_PICTURES;

@Service
@Slf4j
public class DishServiceImp implements DishService {

    @Autowired
    ImageService imageService;

    @Autowired
    DishRepository dishRepository;

    public Dish saveDish(AddDishForm addDishForm, Cook cook) {
        return dishRepository.save(addDishForm.toDish(new Dish(), cook));
    }

    public Dish replaceDish(AddDishForm addDishForm, Cook cook, long dishId) {
        Dish dish = addDishForm.toDish(new Dish(), cook);
        dish.setId(dishId);
        return dishRepository.save(dish);
    }

    /**
     *
     * @param changeDishForm empty
     * @param dish
     * @return populated with the values of dish and the appropriate image
     */
    public ChangeDishForm fromDish(ChangeDishForm changeDishForm, Dish dish) {

        changeDishForm.setDescription(dish.getDescription());
        changeDishForm.setPriceInEuro(dish.getPriceInEuro());
        changeDishForm.setTimeToPrepareInMinutes(dish.getTimeToPrepareInMinutes());
        changeDishForm.setTitle(dish.getTitle());
        changeDishForm.setQuantity(dish.getQuantity());


        String imageName = PREFIX_SMALL_PICTURES
                + PREFIX_DISH + dish.getId();

        log.debug("Image name:" + imageName);

        String image = null;
        try {
            image = imageService.readBase64OfImage(imageName);
        } catch (IOException e) {
            log.error("This should never happen. Image is compulsory when adding dishes");
            log.error(e.toString());
        }

        changeDishForm.setImageInBase64(image);

        return changeDishForm;
    }



}
