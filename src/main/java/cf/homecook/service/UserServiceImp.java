package cf.homecook.service;

import cf.homecook.dto.RegisterForm;
import cf.homecook.entity.Client;
import cf.homecook.entity.Cook;
import cf.homecook.entity.User;
import cf.homecook.repo.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@Slf4j
public class UserServiceImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public boolean isEmailUsed(User user) {
        return isEmailUsed(user.getEmail());
    }

    public boolean isEmailUsed(String email) {
        return userRepository.findByEmail(email)
                .map(user -> true)
                .orElse(false);
    }

    private User formToUser(User user, RegisterForm registerForm) {

        if(registerForm.getRole().equals(User.ROLE_COOK))
            user = new Cook();
        else if(registerForm.getRole().equals(User.ROLE_CLIENT))
            user = new Client();
        else
            log.error("This musn't happen. Role should be either client or cook");

        user.setCreatedAt(Instant.now().toEpochMilli());
        user.setEmail(registerForm.getEmail());
        // when we transform from the register form the user is unregistered
        // hence the default value of false for enabled
        user.setEnabled(false);
        user.setEncryptedPassword(passwordEncoder.encode(registerForm.getUnencryptedPassword()));
        user.setFirstName(registerForm.getFirstName());
        user.setLastName(registerForm.getLastName());
        user.setRole(registerForm.getRole());
        user.setLatitude(registerForm.getLatitude());
        user.setLongitude(registerForm.getLongitude());

        return user;
    }

    public User formToUser(RegisterForm registerForm) {
        return formToUser(new User(), registerForm);
    }


}
