package cf.homecook.service;

import cf.homecook.dto.PurchaseWithAddress;
import cf.homecook.entity.Address;
import cf.homecook.entity.OrderDish;
import cf.homecook.repo.AddressRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PurchaseWithAddressServiceImp implements PurchaseWithAddressService {

    @Autowired
    AddressRepository addressRepository;

    public void findAndPutAddress(PurchaseWithAddress purchaseWithAddress) {

        List<OrderDish> orderDishList = purchaseWithAddress.getPurchase().getItems();
        List<Address> addresses = purchaseWithAddress.getAddressForDishes();

        for(int i = 0; i < orderDishList.size(); i++) {

            long cookId = orderDishList.get(i).getDish().getCook().getId();
            Optional<Address> cookAddressOptional = addressRepository.findAddressByUserId(cookId);

            if(cookAddressOptional.isEmpty()) {
                log.error("This musnt't happen. Every cook must have exactly one address");
            }

            Address address = cookAddressOptional.get();

            addresses.add(i, address);
        }


    }


}
