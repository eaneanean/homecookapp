package cf.homecook.service;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface TokenService {

    public Token createToken(User user);
    public int saveToken(Token token);
    public Optional<Token> findTokenByTokenText(String token);
}
