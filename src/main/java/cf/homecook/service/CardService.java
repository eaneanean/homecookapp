package cf.homecook.service;

import cf.homecook.dto.AddToCard;
import cf.homecook.dto.PurchaseWithAddress;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CardService {
    public OrderDish orderDishFromAddToCard(OrderDish orderDish, AddToCard addToCard, Purchase purchase);
    public void makePurchase(List<AddToCard> addToCardList, Purchase savedPurchase);
    public PurchaseWithAddress makePayment(Purchase purchase, List<AddToCard> addToCardList);
}
