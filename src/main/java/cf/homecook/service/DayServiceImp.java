package cf.homecook.service;

import org.springframework.stereotype.Service;

@Service
public class DayServiceImp implements DayService {


    public String dayToDayName(int dayNumber)  {
        if(dayNumber == 0)
            return "Monday";
        else if(dayNumber == 1)
            return "Tuesday";
        else if(dayNumber == 2)
            return  "Wednesday";
        else if(dayNumber == 3)
            return "Thursday";
        else if(dayNumber == 4)
            return "Friday";
        else if(dayNumber == 5)
            return "Saturday";
        else if(dayNumber == 6)
            return "Sunday";
        else
            throw new RuntimeException("Day must be between 0(included) and 6(included) and " +
                    "this is: " + dayNumber);
    }


}
