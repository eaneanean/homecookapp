package cf.homecook.service;

import cf.homecook.dto.RegisterForm;
import cf.homecook.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    public boolean isEmailUsed(User user);
    public boolean isEmailUsed(String email);
    User formToUser(RegisterForm registerForm);

}
