package cf.homecook.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public interface ImageService {

    public boolean saveOriginalImage(MultipartFile image, String imageName) throws IOException;
    public void saveImage(MultipartFile image, String imageName) throws IOException;
    public void saveSmallImage(MultipartFile image, String imageName) throws IOException;
    public byte [] readImage(String imageName) throws IOException;
    public String readBase64OfImage(String imageName) throws  IOException;

}
