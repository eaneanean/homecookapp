package cf.homecook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * Orders made to the principal cook(cook who is currently logged in)
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CookOrder {

    private String fname;
    private String lname;
    // chosen time either for delivery or for
    // picking up
    private Timestamp time;
    private boolean delivery;
    private String title;
    private int quantity;


}
