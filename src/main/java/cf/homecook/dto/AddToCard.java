package cf.homecook.dto;

import cf.homecook.entity.Dish;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import cf.homecook.entity.User;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

// used in the cookie
@Data
public class AddToCard {

    private long dishId;
    private String title;
    private int availableQuantity;
    private double price;
    private int quantity;
    private int hour;
    private int minute;
    private boolean delivery;
    String date;
    private String image;


//    @Id
//    @GeneratedValue(generator = "order_dish_seq")
//    private long id;
//
//    @ManyToOne
//    private Dish dish;
//
//    @ManyToOne
//    private Purchase purchase;
//
//    private int quantity;
//    private double pricePerDishInEuros;
//    private boolean isDelivery;


    public boolean getDelivery() {
        return delivery;
    }


    public OrderDish toOrderDish(OrderDish orderDish, Purchase purchase) {




        return null;
    }


}
