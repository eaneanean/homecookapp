package cf.homecook.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SelectAddressForm {

    long addressId;
}
