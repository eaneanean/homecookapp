package cf.homecook.dto;

import cf.homecook.entity.WorkingDay;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class DaysForm {

    List<WorkingDayDto> list;
}
