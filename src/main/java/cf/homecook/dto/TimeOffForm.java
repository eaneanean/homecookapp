package cf.homecook.dto;

import cf.homecook.entity.TimeOff;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.time.LocalDateTime;


import static cf.homecook.utils.Common.addZero;
@Data
@NoArgsConstructor
@Slf4j
public class TimeOffForm {

    // the first of May 2021 will be written as: 2021-05-01
    private String fromDate;
    private int fromHour;
    private int fromMinute;

    private String untilDate;
    private int untilHour;
    private int untilMinute;

    /**
     *
     * @param timeOff empty, except the value of cook
     * @return populated with values of this
     */
    public TimeOff toTimeOff(TimeOff timeOff) {

        String from = fromDate + " " + addZero(fromHour) + ":" + addZero(fromMinute) + ":" + "00";
        String until = untilDate + " " + addZero(untilHour) + ":" + addZero(untilMinute) + ":" + "00";

        log.debug("from:" + from);
        log.debug("until:" + until);

        Timestamp fromTimestamp = Timestamp.valueOf(from);
        Timestamp untilTimeStampt = Timestamp.valueOf(until);

        timeOff.setFromTime(fromTimestamp);
        timeOff.setUntilTime(untilTimeStampt);

        return timeOff;
    }

    public void setFromAndUntil(LocalDateTime from, LocalDateTime until) {

        this.fromDate = from.toLocalDate().toString();
        this.untilDate = until.toLocalDate().toString();

        this.fromHour = from.getHour();
        this.untilHour = until.getHour();

        this.fromMinute = from.getMinute();
        this.untilMinute = until.getMinute();

    }


}
