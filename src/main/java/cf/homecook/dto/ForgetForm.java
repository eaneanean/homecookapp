package cf.homecook.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class ForgetForm {

    @NonNull
    String email;

}