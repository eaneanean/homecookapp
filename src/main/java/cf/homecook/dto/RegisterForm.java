package cf.homecook.dto;

import cf.homecook.entity.Client;
import cf.homecook.entity.Cook;
import cf.homecook.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;

@Data
@NoArgsConstructor
@Slf4j
public class RegisterForm {

    @NonNull
    @NotNull
    @Size(min = 2, max = 50)
    private String firstName;

    @NonNull
    @NotNull
    @Size(min = 2, max = 50)
    private String lastName;

    @NonNull
    @NotNull
    @Email
    private String email;

    private double latitude;

    private double longitude;

    @NotNull
    private String unencryptedPassword;

    @NotNull
    private String unencryptedRepeatedPassword;

    @NotNull
    String role;


}
