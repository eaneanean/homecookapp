package cf.homecook.dto;

import cf.homecook.entity.Address;
import cf.homecook.entity.Dish;
import cf.homecook.entity.OrderDish;
import cf.homecook.entity.Purchase;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.query.criteria.internal.OrderImpl;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PurchaseWithAddress {

    Purchase purchase;

    // address that corresponds to the dish
    // of the OrderDish items in the purchase.
    // The list has same ordering as the dishes: for
    // example the corresponding address for orderDish.get(0)
    // is addressForDishes(0), or for the orderDish.get(5)
    // is addressForDishes(5)
    List<Address> addressForDishes = new ArrayList<>();

    public String toString() {

        StringBuilder sb = new StringBuilder();

        List<OrderDish> list  = purchase.getItems();

        String euro = "\u20ac";

        for(int i = 0; i < list.size(); i++) {

            OrderDish orderDish = list.get(i);
            Dish dish = orderDish.getDish();
            sb.append(dish.getTitle());
            sb.append(" ");
            sb.append(orderDish.getPricePerDishInEuros());
            sb.append(euro);
            sb.append(" ");
            sb.append(orderDish.getQuantity());


            if(orderDish.isDelivery()) {
                sb.append(" with delivery at address ");
                sb.append(purchase.getAddress());
            }
            else {
                sb.append(" you must pick it up at ");
                sb.append(addressForDishes.get(i).toString());
            }

            sb.append(" " + orderDish.getChosenTime());
            sb.append("\n");
        }

        return sb.toString();

    }

}
