package cf.homecook.dto;

import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@Slf4j
public class AddDishForm {

    @NonNull
    @NotNull
    @Size(min = 2, max = 50)
    String title;

    @NonNull
    @NotNull
    @Size(min = 50, max = 2000)
    String description;


    double priceInEuro;

    @Min(5)
    @Max(720)
    int timeToPrepareInMinutes;

    MultipartFile imageFile;

    int quantity;

    /**
     *
     * @param dish empty dish, created with new Dish()
     * @param cook the cook for this dish
     * @return
     */
    public Dish toDish(Dish dish, Cook cook) {

        dish.setTitle(title);
        dish.setDescription(description);
        dish.setPriceInEuro(priceInEuro);
        dish.setTimeToPrepareInMinutes(timeToPrepareInMinutes);
        dish.setCook(cook);
        dish.setQuantity(quantity);

        return dish;
    }


}
