package cf.homecook.dto;

import cf.homecook.entity.Cook;
import cf.homecook.entity.WorkingDay;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Time;

import static cf.homecook.utils.Common.addZero;
@Data
@NoArgsConstructor
@Slf4j
public class WorkingDayDto {

    private long id;

    /**
     *  0 - Monday
     *  1 - Tuesday
     *  2 - Wednesday
     *  3- Thursday
     *  4 - Friday
     *  5 - Saturday
     *  6 - Sunday
     */
    private int dayOfTheWeek;

    @ManyToOne
    private Cook cook;

    private int fromTimeHour;
    private int fromTimeMinutes;

    private int untilTimeHour;
    private int untilTimeMinutes;

    // this is boolean instead of using the absence
    // of values in the fromTime and untilTime column
    // because we want to able not to lose information when
    // the user often flips whether it is a working day
    private boolean isWorkDay;

    public boolean getIsWorkDay() {
        return  isWorkDay;
    }

    public void setIsWorkDay(boolean bool) {
        this.isWorkDay = bool;
    }

    private String dayName;

    /**
     *
     * @param day empty
     * @return filled with values from this dto
     */
    public WorkingDay toWorkingDay(WorkingDay day) {

        log.debug("From time hour:" + fromTimeHour + ",from time minutes:" + fromTimeMinutes
        + ",until time hour " + untilTimeHour + ", until time minutes: " + untilTimeMinutes);

        day.setCook(cook);
        day.setWorkDay(isWorkDay);
        String fromTime = addZero(fromTimeHour) + ":" + addZero(fromTimeMinutes) + ":00";
        String untilTime = addZero(untilTimeHour) + ":" + addZero(untilTimeMinutes) + ":00";

        log.debug("From:" + fromTime);
        log.debug("Until:" + untilTime);

        day.setFromTime(Time.valueOf(fromTime));
        day.setUntilTime(Time.valueOf(untilTime));
        day.setId(id);
        day.setDayOfTheWeek(dayOfTheWeek);

        return day;
    }


    public WorkingDayDto fromWorkingDay(WorkingDay day) {
        Time until = day.getUntilTime();
        Time from = day.getFromTime();

        this.id = day.getId();
        this.cook = day.getCook();
        this.isWorkDay = day.isWorkDay();
        this.dayOfTheWeek = day.getDayOfTheWeek();
        this.fromTimeHour = from.getHours();
        this.fromTimeMinutes = from.getMinutes();
        this.untilTimeHour = until.getHours();
        this.untilTimeMinutes = until.getMinutes();

        return this;
    }

}
