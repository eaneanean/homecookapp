package cf.homecook.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ChangeDishForm extends AddDishForm {

    public String imageInBase64;

}
