package cf.homecook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    @NonNull
    private double latitude;
    @NonNull private double longitude;

}
