package cf.homecook.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
// used in /browse to show the dishes
public class DishDto {

    @NonNull
    long dishId;

    @NonNull
    long cookId;

    @NonNull
    double distance;

    @NonNull
    int time;

    @NonNull
    String title;

    @NonNull
    String firstName;

    @NonNull
    String lastName;

    @NonNull
    double average;

    @NonNull
    int total;

    @NonNull
    double price;

    String image;

    public DishDto(@NonNull long dishId, @NonNull long cookId, @NonNull double distance, @NonNull int time, @NonNull String title, @NonNull String firstName, @NonNull String lastName, @NonNull double average, @NonNull int total, @NonNull double price) {
        this.dishId = dishId;
        this.cookId = cookId;
        this.distance = distance;
        this.time = time;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.average = average;
        this.total = total;
        this.price = price;
    }
}
