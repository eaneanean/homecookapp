package cf.homecook.dto;

import cf.homecook.entity.Cook;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CookProfileDto {

    @Size(min = 50, max = 2000)

    private String description;
    private String image;

    private MultipartFile imageFile;

}
