package cf.homecook.controller;

import cf.homecook.dto.ForgetForm;
import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.ConformationService;
import cf.homecook.service.TokenService;
import cf.homecook.service.UserService;
import cf.homecook.validator.ForgetFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Locale;

@Controller
/**
 * Handles forget password form
 */
public class ForgetController {


    @Autowired
    ForgetFormValidator forgetFormValidator;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TokenService tokenService;

    @Autowired
    ConformationService conformationService;


    @GetMapping("/forget")
    public String handleGetRequest(Model model, Locale locale) {

        model.addAttribute(new ForgetForm());

        return "forget";
    }

    @PostMapping("/forget")
    public String handlePostRequest(@Valid ForgetForm forgetForm, BindingResult bindingResult,
                                    Locale locale, HttpServletRequest request) throws MalformedURLException, URISyntaxException {


        forgetFormValidator.validate(forgetForm, bindingResult);


        if(bindingResult.hasErrors()) {
            System.out.println("Greska");
            return "forget";
        }

        // it is guaranteed to have value, because we checked with the
        // forgetFormValidator
        User user = userRepository.findByEmail(forgetForm.getEmail()).get();
        Token token = tokenService.createToken(user);
        tokenService.saveToken(token);

        conformationService.sendChangePassword(forgetForm.getEmail(),
                token,
                conformationService.getURI(request, "/password"));

        return "redirect:/login";


    }

}
