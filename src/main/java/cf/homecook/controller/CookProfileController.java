package cf.homecook.controller;

import cf.homecook.dto.CookProfileDto;
import cf.homecook.entity.Cook;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.ImageService;
import cf.homecook.service.ImageServiceHelper;
import cf.homecook.validator.ImageValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

import static cf.homecook.service.ImageServiceHelper.PREFIX_COOK;
import static cf.homecook.service.ImageServiceHelper.PREFIX_DISH;

@Controller
@RequestMapping("/cook/profile")
@Slf4j
public class CookProfileController {

    @Autowired
    ImageService imageService;

    @Autowired
    ImageValidator imageValidator;

    @Autowired
    UserRepository userRepository;

    @GetMapping
    public String handleGet(@AuthenticationPrincipal Cook cook, Model model) {

        log.debug("cook description:" + cook.getDescription());

        String image = "";

        try {
            image = imageService.readBase64OfImage(ImageServiceHelper.PREFIX_SMALL_PICTURES +
                    PREFIX_COOK  + cook.getId());
        } catch (IOException e) {
            log.debug("Image not found. The cook hasn't uploaded a picture yet.");
        }

        CookProfileDto profile = new CookProfileDto();
        profile.setDescription(cook.getDescription());
        profile.setImage(image);

        if(image.equals("") || image.isEmpty())
            model.addAttribute("showPicture", false);
        else
            model.addAttribute("showPicture", true);

        model.addAttribute("profile", profile);

        return "cookProfile";
    }


    @PostMapping
    public String handlePost(@AuthenticationPrincipal Cook cook, @Valid CookProfileDto profileDto,
                             BindingResult bindingResult) {

        MultipartFile image = profileDto.getImageFile();

        // it's okay here image to be empty because it only means
        // that user hasn't entered a new image
        if(!image.isEmpty()) {
            log.debug("image is NOT empty");
            imageValidator.validate(image, bindingResult);
        }
        else
            log.debug("image is empty");

        if(bindingResult.hasErrors()) {
            bindingResult
                    .getAllErrors()
                    .stream()
                    .forEach(objectError -> log.debug(objectError.toString()));
            return "cookProfile";
        }

        if(!image.isEmpty()) {
            String imageName =  PREFIX_COOK + cook.getId();

            log.debug("Image name:" + imageName);

            try {
                imageService.saveImage(image, imageName);
            } catch (IOException e) {
                log.error(e.toString());
            }
        }

        cook.setDescription(profileDto.getDescription());
        userRepository.save(cook);



        return "redirect:/cook/browse";
    }


}
