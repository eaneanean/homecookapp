package cf.homecook.controller;

import cf.homecook.entity.Address;
import cf.homecook.entity.Client;
import cf.homecook.entity.Cook;
import cf.homecook.entity.User;
import cf.homecook.repo.AddressRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
public class AddressController {

    @Autowired
    AddressRepository addressRepository;

    @GetMapping(value = {"/address/{id}", "/address"})
    public String handleGet(@PathVariable("id") Optional<Long> id, Model model,
                            @AuthenticationPrincipal User principal) {

        Address address = new Address();

        if(id.isPresent()) {
            Optional<Address> optionalAddress = addressRepository.findById(id.get());
            if(optionalAddress.isPresent()) {
                Address currentAddress = optionalAddress.get();

                // check whether the address with the given id
                // belongs to the currently logged in user. If it does it
                // fills the of address object from the db(from the optionalAddress
                // object). Otherwise, the address will be empty
                if(currentAddress.getUser().getId() == principal.getId())
                    address = optionalAddress.get();
            }
        }

        model.addAttribute("address", address);

        return "address";
    }

    @GetMapping("myaddress")
    /**
     * All the address that the logged in user previously entered.
     */
    public String handleMyAddresses(@AuthenticationPrincipal Client client,
                                            Model model) {
        List<Address> addresses = addressRepository.findAddressesByUserId(client.getId());

        model.addAttribute("addresses", addresses);
        return "addresses";
    }

    @PostMapping("/address")
    public String handlePost(Address address, @AuthenticationPrincipal User principal) {

        log.debug("POST /address");

        address.setUser(principal);
        addressRepository.save(address);
        return "redirect:/myaddress";

    }

    @DeleteMapping("/myaddress")
    @ResponseBody
    public String handleDelete(long addressId, @AuthenticationPrincipal User user) {
        Optional<Address> optionalAddress = addressRepository.findById(addressId);
        if(optionalAddress.isPresent()) {
            Address address = optionalAddress.get();
            // if the requested address to delete(see below how delete is defined) belongs to the
            // logged in user
            if(address.getUser().getId() == user.getId())
                // we don't really delete the address because we may
                // need it for the order. We just make the user null
                address.setUser(null);
                addressRepository.save(address);
                return "deleted";
        }
        log.debug("DELETE method called");
        return "not deleted";
    }

    @GetMapping("/cook/address")
    // the difference between a client and a cook is that a
    // client can have multiple address and can choose which one
    // should be selected when making an order. The cook on the
    // other hand can only have one address.
    public String handleCookGet(@AuthenticationPrincipal Cook cook, Model model) {

        log.debug("/cook/address GET called");

        Optional<Address> optionalAddress = addressRepository.findAddressByUserId(cook.getId());

        Address address = new Address();

        // user have already entered the address. Remember, cook
        // can have only one address
        if(optionalAddress.isPresent()) {
            address = optionalAddress.get();
            model.addAttribute("address");
        }


        model.addAttribute("address", address);
        return "cookAddress";
    }


    @PostMapping("/cook/address")
    public String handleCookPost (@AuthenticationPrincipal User user, Address address){
        log.debug("POST cook/address");
        log.debug("Address:" + address);

        address.setUser(user);
        addressRepository.save(address);

        return "redirect:/cook/browse";
    }

}
