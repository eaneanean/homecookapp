package cf.homecook.controller;

import cf.homecook.dto.DishDto;
import cf.homecook.entity.Client;
import cf.homecook.entity.Dish;
import cf.homecook.entity.DishRating;
import cf.homecook.entity.User;
import cf.homecook.repo.DishDtoRepositoryImp;
import cf.homecook.repo.DishRatingRepository;
import cf.homecook.repo.DishRepository;
import cf.homecook.repo.OrderDishRepository;
import cf.homecook.service.ConformationService;
import cf.homecook.service.ImageService;
import cf.homecook.service.ImageServiceHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import static cf.homecook.service.ImageServiceHelper.*;

import static cf.homecook.utils.Common.addZero;
@Controller
@RequestMapping({"/browse"})
@Slf4j
public class BrowseDishUserController {

    @Autowired
    DishRepository dishRepository;

    @Autowired
    ImageService imageService;

    @Autowired
    DishDtoRepositoryImp dishDtoRepositoryImp;

    @Autowired
    OrderDishRepository orderDishRepository;

    @Autowired
    DishRatingRepository dishRatingRepository;

    @Autowired
    ConformationService conformationService;



    @GetMapping
    public String handleGet(@RequestParam(defaultValue = "1") int page,
                            @RequestParam(defaultValue = "") String searchWord,
                            @RequestParam(defaultValue = "dist_asc") String sort,
                            @RequestParam(defaultValue = "20") int distance,
                            @RequestParam(defaultValue = "today") String chosenDate,
                            @RequestParam(defaultValue = "in1Hour") String chosenTimeHour,
                            @RequestParam(defaultValue = "in1Hour") String chosenTimeMinutes,
                            Model model,
                            @AuthenticationPrincipal User userPrincipal,
                            HttpServletRequest request) {

        LocalDateTime now = LocalDateTime.now();

        log.debug("Search word:" + searchWord);

        // the default time, this date and time plus one hour
        LocalDateTime inHour = now.plusHours(1);

        String chosenTime = "";

        // if the value of chosenDate is today that means
        // that that user opens the default page, that means
        // without filtering the data
        if(chosenDate.equals("today")) {
            chosenDate = inHour.toLocalDate().toString();
            chosenTime = inHour.toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString();
            chosenTimeHour = String.valueOf(inHour.toLocalTime().truncatedTo(ChronoUnit.SECONDS).getHour());
            chosenTimeMinutes = String.valueOf(inHour.toLocalTime().truncatedTo(ChronoUnit.SECONDS).getMinute());
        }
        else {
            chosenTime = addZero(chosenTimeHour) + ":" + addZero(chosenTimeMinutes) + ":00";
        }

        log.debug("chosenDate:" + chosenDate);
        log.debug("chosenTime:" + chosenTime);

        String timeNowSqlFormated = now.toLocalDate().toString() + " " +
                now.toLocalTime().truncatedTo(ChronoUnit.SECONDS).toString();
        log.debug("time now sql formated:" + timeNowSqlFormated);

        List<DishDto> dishDtoList =  dishDtoRepositoryImp.find(userPrincipal.getId(), searchWord, sort, distance,
                chosenDate, chosenTime, timeNowSqlFormated);


        dishDtoList.stream().forEach(e-> log.debug(e.toString()));

        PagedListHolder<DishDto> holder = new PagedListHolder<>(dishDtoList);
        holder.setPageSize(6);
        holder.setPage(page - 1);

        boolean isThereNext = !holder.isLastPage();
        boolean isTherePrevious = !holder.isFirstPage();

        List<DishDto> pageResult = holder.getPageList();

        for(int i = 0; i < pageResult.size(); i++) {
            try {
                DishDto currentDishDto = pageResult.get(i);
                String image = imageService.readBase64OfImage(PREFIX_SMALL_PICTURES + PREFIX_DISH +
                        currentDishDto.getDishId());
                currentDishDto.setImage(image);

            } catch (IOException e) {
                log.error(e.toString());
            }
        }

        String url = "/browse?sort="+ sort + "&" + "searchWord=" + searchWord + "&chosenDate=" + chosenDate
                + "&chosenTimeHour=" + chosenTimeHour + "&chosenTimeMinutes=" + chosenTimeMinutes
                + "&distance=" + distance + "&page=";

        String urlForSearch = null;
        try {
            String uri = conformationService.getURI(request, "");
            log.debug("uri:" + uri);
            urlForSearch = uri + "/browse?sort="+ sort + "&" + "&chosenDate=" + chosenDate
                    + "&chosenTimeHour=" + chosenTimeHour + "&chosenTimeMinutes=" + chosenTimeMinutes
                    + "&distance=" + distance;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        log.debug("url:" + url);

        int previosPage = page - 1;
        int nextPage = page + 1;

        String previousPageUrl = url + previosPage;
        String nextPageUrl = url + nextPage;

        log.debug("previous page url:" + previousPageUrl);
        log.debug("next page url:" + nextPageUrl);

        String dateTimeUrl = "hour=" + chosenTimeHour + "&" +
                "minute=" + chosenTimeMinutes + "&date=" + chosenDate;

        model.addAttribute("untilMinute", chosenTimeMinutes);
        model.addAttribute("untilHour", chosenTimeHour);
        model.addAttribute("chosenDate", chosenDate);
        model.addAttribute("distance", distance);
        model.addAttribute("dateTimeUrl", dateTimeUrl);
        model.addAttribute("urlForSearch", urlForSearch);

        model.addAttribute("dishList", pageResult);
        model.addAttribute("isTherePrevious", isTherePrevious);
        model.addAttribute("isThereNext", isThereNext);
        model.addAttribute("previous", previousPageUrl);
        model.addAttribute("next", nextPageUrl);
        model.addAttribute("searchWord", searchWord);

        return "browseDishUser";
    }



    @GetMapping("/{dishId}")
    public String handleGetId(
            @RequestParam(value = "hour", required = false, defaultValue = "-1") int hour,
            @RequestParam(value =  "minute", required = false, defaultValue = "-1") int minute,
            @RequestParam(value = "date", required = false, defaultValue = "today") String date,
            @PathVariable("dishId") long dishId, Model model,
            @AuthenticationPrincipal User user) {
        Optional<Dish> optionalDish = dishRepository.findById(dishId);

        if(optionalDish.isPresent()) {

            Dish dish = optionalDish.get();
            int numberOfTimesDishWasOrdered =  orderDishRepository.numberOfTimesDishWasOrdered(dish.getId(), user.getId());

            model.addAttribute("dish", dish);

            Optional<DishRating> dishRatingOptional = dishRatingRepository.findByClientIdAndDishId(user.getId(), dish.getId());

            if(dishRatingOptional.isPresent()) {
                int stars = dishRatingOptional.get().getStars();
                log.debug("Dish was rated with stars:" + stars);
                model.addAttribute("stars", stars);
                model.addAttribute("shouldShow", true);
            }

            else if(numberOfTimesDishWasOrdered >= 1) {
                model.addAttribute("shouldShow", true);
            }
            else
                model.addAttribute("shouldShow", false);

            try {
                String image =
                        imageService.readBase64OfImage(PREFIX_DISH + dish.getId());
                model.addAttribute("image", image);
            } catch (IOException e) {
                log.error("This must not happen. The dish exists and " +
                        "dish in order to be save must have a valid picture");
                log.error(e.toString());
            }


            return "dish";
        }
        else {
            model.addAttribute("message", "Dish with id " +
                    dishId + " does not exist");
            return "error";
        }

    }

    @PostMapping("/rateDish")
    @ResponseBody
    public String handleRestPost(int stars, long dishId, @AuthenticationPrincipal Client client ) {
        String text = "/browse/rateDish stars:" + stars + " dishId:" + dishId;
        int numberOfTimesDishWasOrdered =  orderDishRepository.numberOfTimesDishWasOrdered(dishId
                , client.getId());

        // you cannot rate something that haven't ordered at least once
        if(numberOfTimesDishWasOrdered < 1)
            return "Can't rate something you haven't ordered";

        Optional<DishRating> dishRatingOptional = dishRatingRepository.findByClientIdAndDishId(client.getId(), dishId);

        // user already gave a rating and the rating should be updated with the
        // new stars and the new time
        if(dishRatingOptional.isPresent()) {
            DishRating dishRating = dishRatingOptional.get();
            dishRating.setCreatedAt(System.currentTimeMillis());
            dishRating.setStars(stars);
            dishRatingRepository.save(dishRating);
            return "updated with new value";
        }
        else {
            // if it is first time
            DishRating dishRating = new DishRating();
            Optional<Dish> optionalDish = dishRepository.findById(dishId);
            if(optionalDish.isEmpty())
                return "can't rate non-existent dish";

            Dish dish = optionalDish.get();
            dishRating.setDish(dish);
            dishRating.setStars(stars);
            dishRating.setCreatedAt(System.currentTimeMillis());
            dishRating.setClient(client);
            dishRatingRepository.save(dishRating);
            return "completely new rating inserted";
        }

    }


}
