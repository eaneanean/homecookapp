package cf.homecook.controller;

import cf.homecook.entity.Client;
import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import cf.homecook.entity.User;
import cf.homecook.repo.DayRepository;
import cf.homecook.repo.DishRepository;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.EmailService;
import cf.homecook.service.MyUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.validation.constraints.Email;
import java.util.Optional;
import java.util.TimeZone;

@Controller
@Slf4j
public class TestController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private SpringTemplateEngine thymeleafTemplateEngine;

    @Autowired
    EmailService emailService;

    @Autowired
    MyUserDetailsService myUserDetailsService;

    @Autowired
    DayRepository dayRepository;

    @GetMapping("userExists/{email}")
    @ResponseBody
    public void handleUserExist(@PathVariable("email") String email) {
        try {
            System.out.println(myUserDetailsService.loadUserByUsername(email));
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }

    @GetMapping("/home")
    public String handleHome() {
        return "redirect:/browse";
    }

    @GetMapping("/engine")
    @ResponseBody
    public String handleEngine() {
        Context thymeleafContext = new Context();

        String htmlBody = thymeleafTemplateEngine.process("addDay.html", thymeleafContext);
        return htmlBody;

    }

    @GetMapping("/{id}")
    public String handleGett(@PathVariable("id") long id) {

        emailService.sendSimpleMessage("ivan.jelik.c@gmail.com", "Zdravo", "Kako si?");

        return "loginSuccess";
    }

    @Autowired
    DishRepository dishRepo;

    @GetMapping("/dish/{id}")
    public String handleDish(@PathVariable("id") long id, TimeZone timeZone) {


        log.debug("Timezone:" + timeZone);
        log.debug("/dish/{id} called with id:" + id);

        Optional<Dish> dish = dishRepo.findById(id);

        dish.ifPresent(disho -> System.out.println(disho.getRatings().get(0).getStars()));

        return "loginSuccess";
    }

    @GetMapping("/now")
    @ResponseBody
    public String now() {
        String date = jdbcTemplate.queryForObject("SELECT NOW();", String.class);
        log.debug("Time and date from sql:" + date);
        return date;
    }


    @GetMapping("/time")
    @ResponseBody
    public String handleTime(TimeZone timeZone) {
        return timeZone.toString();
    }

    @GetMapping("/cook/delete")
    @ResponseBody
    public long delete(@AuthenticationPrincipal Cook cook) {
        return dayRepository.deleteByCookId(cook.getId());
    }

    @GetMapping("/proba")
    public String proba(@AuthenticationPrincipal User principal) {
        if(principal instanceof Client)
            return "redirect:/browse";
        else
            return "redirect:/cook/browse";
    }


}
