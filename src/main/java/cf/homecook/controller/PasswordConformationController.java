package cf.homecook.controller;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.TokenService;
import cf.homecook.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Optional;

@Slf4j
@Controller
/**
 * Change password by confirming the token sent to email
 */
public class PasswordConformationController {

    @Autowired
    TokenService tokenService;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;



    @GetMapping("/password/{token}")
    public String handleGetRequest(@PathVariable("token") String tokenString, Locale locale, Model model,
                                   HttpServletRequest request) {

        try {
            Optional<Token> optionalToken = tokenService.findTokenByTokenText(tokenString);

            if(optionalToken.isEmpty()) {
                model.addAttribute("message", "Non-existent token");
                return "error";
            }

            Token token = optionalToken.get();


            if(token.isTokenExpired(1000*60*60)) {
                model.addAttribute("errorMessage", "Token was expired. You had one hour to click one the link");
                return "invalidToken";
            }

            User user = userRepository.findById(token.getUser().getId()).get();
            model.addAttribute(user);
            log.debug("User from conformation controller:" + user);
            request.getSession().setAttribute("USER", user);
            return "redirect:/changePassword";
        }
        catch (EmptyResultDataAccessException exception) {
            return "invalidToken";
        }

    }


}
