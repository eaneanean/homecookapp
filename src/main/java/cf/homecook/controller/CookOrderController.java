package cf.homecook.controller;

import cf.homecook.dto.CookOrder;
import cf.homecook.entity.Cook;
import cf.homecook.repo.CookOrderRepository;
import cf.homecook.service.ConformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
public class CookOrderController {

    @Autowired
    CookOrderRepository cookOrderRepository;

    @Autowired
    ConformationService conformationService;

    @GetMapping(value = {"/cook/orders/{page}", "/cook/orders"})
    public String handleGet(@AuthenticationPrincipal Cook cook,
                                     @PathVariable("page") Optional<Integer> page,
                                     HttpServletRequest request, Model model) {
        List<CookOrder> cookOrders = cookOrderRepository.findAllCookOrders(cook.getId());

        int requestPage = 1;

        if(page.isPresent())
            requestPage = page.get();


        PagedListHolder<CookOrder> holder = new PagedListHolder<>(cookOrders);
        holder.setPageSize(10);
        holder.setPage(requestPage - 1);

        boolean isThereNext = !holder.isLastPage();
        boolean isTherePrevious = !holder.isFirstPage();

        int previosPage = requestPage - 1;
        int nextPage = requestPage + 1;

        String url = null;
        try {
            String uri = conformationService.getURI(request, "");
            log.debug("uri:" + uri);
            url = uri + "/cook/orders/";
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String previousPageUrl = url + String.valueOf(requestPage - 1);
        String nextPageUrl = url + String.valueOf(requestPage + 1);


        model.addAttribute("isTherePrevious", isTherePrevious);
        model.addAttribute("isThereNext", isThereNext);
        model.addAttribute("previous", previousPageUrl);
        model.addAttribute("next", nextPageUrl);
        model.addAttribute("cookOrders", holder.getPageList());

        log.debug("Page:" + requestPage);

        return "cookOrders";
    }
}
