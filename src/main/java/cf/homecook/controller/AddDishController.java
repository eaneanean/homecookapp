package cf.homecook.controller;

import cf.homecook.dto.AddDishForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import cf.homecook.service.DishService;
import cf.homecook.service.ImageService;
import cf.homecook.service.ImageServiceHelper;
import cf.homecook.validator.AddDishFormValidator;
import cf.homecook.validator.ImageValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@RequestMapping("cook/addDish")
@Controller
@Slf4j
public class AddDishController {

    @Autowired
    AddDishFormValidator addDishFormValidator;

    @Autowired
    ImageService imageService;

    @Autowired
    ImageValidator imageValidator;

    @Autowired
    DishService dishService;

    @GetMapping
    public String handleGet(Model model) {
        model.addAttribute(new AddDishForm());
        return "addDish";
    }

    @PostMapping
    // it's really important the arguments of this method to be in this order:
    // the RequestParam annotation, form and the binding result. Otherwise, this
    // won't work. Per documentation the form must always be followed by BindingResult,
    // but if you put form before the MultipartFile, the binding will be done even
    // for the image and it will give binding error
    public String handlePost(
                             @Valid AddDishForm addDishForm,
                             BindingResult result,
                             @AuthenticationPrincipal Cook cook) {

        MultipartFile image = addDishForm.getImageFile();

        log.debug(addDishForm.toString());
        log.debug("Id of principal cook:" + cook.getId());

        log.debug(image.getContentType() + "," +
                image.getOriginalFilename() + "," +
                image.getSize());

        addDishFormValidator.validate(addDishForm, result);

        try {
            imageValidator.validate(image, result);
        } catch (Exception e) {
            log.error(e.toString());
        }

        if(result.hasErrors()) {
            result
            .getAllErrors()
            .stream()
            .forEach(objectError -> log.debug(objectError.toString()));
            return "addDish";
        }

        Dish savedDish = dishService.saveDish(addDishForm, cook);

        try {
            imageService.saveImage(image, ImageServiceHelper.PREFIX_DISH + savedDish.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/cook/browse";
    }

}
