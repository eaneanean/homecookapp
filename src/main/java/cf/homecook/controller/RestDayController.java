package cf.homecook.controller;

import cf.homecook.dto.TimeOffForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.TimeOff;
import cf.homecook.repo.TimeOffRepository;
import cf.homecook.validator.TimeOffFormValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

@RequestMapping("cook/off")
@Controller
@Slf4j
public class RestDayController {

    @Autowired
    TimeOffFormValidator validator;

    @Autowired
    TimeOffRepository timeOffRepository;

    @GetMapping
    public String handleGet(Model model) {

        log.debug("cook/off GET method");

        TimeOffForm timeOffForm = new TimeOffForm();


        model.addAttribute("timeOffForm", timeOffForm);

        return "offTime";
    }

    @PostMapping
    public String handlePost(TimeOffForm timeOffForm,
                             BindingResult result,
                             Model model,
                             @AuthenticationPrincipal Cook cook)  {


        log.debug("cook/off POST method");
        log.debug(timeOffForm.toString());

        validator.validate(timeOffForm, result);


        if(result.hasErrors()) {
            log.debug("Result has errors");
            result.getAllErrors()
                    .stream()
                    .forEach(error -> log.debug(error.toString()));
            model.addAttribute("isThereError", true);
            return "offTime";
        }

        log.debug("Result has no errors");

        TimeOff timeOff = new TimeOff();
        timeOff.setCook(cook);

        timeOffForm.toTimeOff(timeOff);

        timeOffRepository.save(timeOff);


        return "redirect:/cook/off";
    }

}
