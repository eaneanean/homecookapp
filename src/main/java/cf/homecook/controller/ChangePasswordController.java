package cf.homecook.controller;

import cf.homecook.dto.ChangePasswordForm;
import cf.homecook.entity.User;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.UserService;
import cf.homecook.validator.ChangePasswordValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@Slf4j
/**
 * Change the password both for logged in user and for users
 * who had previously clicked the link with the token for
 * changing email
 */
public class ChangePasswordController {

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    ChangePasswordValidator validator;

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/changePassword")
    public String handleGetRequest(@AuthenticationPrincipal User userPrincipal,
                                   HttpSession session, Model model) {

        User user;
        User userFromSession = (User) session.getAttribute("USER");

        if(userPrincipal != null) {
            user = userPrincipal;
            log.debug("User principal is not null");
        }
        else if(userFromSession != null) {
            user = userFromSession;
            log.debug("User from session is not null");
        }
        else {
            log.debug("get method /changePassword");
            log.debug(userPrincipal.toString());
            log.debug("User2:" + userFromSession);
            model.addAttribute(new ChangePasswordForm());
            return "changePasswordError";
        }

        model.addAttribute(new ChangePasswordForm());
        model.addAttribute(user);

        return "changePassword";
    }

    @PostMapping("/changePassword")
    public String handlePostRequest(@AuthenticationPrincipal User userPrincipal,
                                    @Valid ChangePasswordForm form, HttpSession session,
                                    BindingResult result) {

        validator.validate(form, result);

        if(result.hasErrors()) {
            return "changePassword";
        }

        User user = new User();
        User userFromSession = (User) session.getAttribute("USER");

        if(userPrincipal != null) {
            user = userPrincipal;
            System.out.println("User principal is not null from POST /changePasswor");
        }
        else if(userFromSession != null) {
            user = userFromSession;
            System.out.println("User from session is not null from POST /changePassword");
        }

        String encryptedPassword = encoder.encode(form.getPassword());

        user.setEncryptedPassword(encryptedPassword);
        userRepository.save(user);

        return "redirect:/browse";
    }

}

