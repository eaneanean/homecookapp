package cf.homecook.controller;

import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import cf.homecook.repo.TokenRepository;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.TokenService;
import cf.homecook.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Optional;

@Controller
/**
 * After the user fills the register, he should get a conformation mail that must be
 * confirmed so that the user can be considered a register user.
 */
@Slf4j
public class ConformationController {

    @Autowired
    TokenService tokenService;

    @Autowired
    UserService userService;

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    UserRepository userRepository;


    @GetMapping("/conformation")
    public String handleConformation() {
        return "conformation";
    }

    @GetMapping("/conformation/{token}")
    public String handleConformationWithTokenString(
            @PathVariable("token") String tokenString, Locale locale, Model model,
            HttpServletRequest request) {

        try {

            Optional<Token> optionalToken = tokenRepository.findByRandomString(tokenString);

            if(optionalToken.isEmpty()) {
                model.addAttribute("message", "Token is not in the database");
                log.debug("Token is not in the database");
                return "error";
            }

            Token token = optionalToken.get();

            if (token.isTokenExpired(1000 * 60 * 60)) {
                model.addAttribute("message", "The token is expired");
                return "error";
            }

            User user = token.getUser();
            user.setEnabled(true);
            userRepository.save(user);
            model.addAttribute(user);
            log.debug("User from conformation controller:" + user);
            request.getSession().setAttribute("USER", user);
            return "redirect:/login";
        }
        catch (EmptyResultDataAccessException exception) {
            return "invalidToken";
        }


    }

}