package cf.homecook.controller;

import cf.homecook.dto.Location;
import cf.homecook.entity.User;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
/**
 * Handles the current location of the user
 */
@Slf4j
public class LocationController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/location")
    public String handelGet(Model model) {

        model.addAttribute("locationForm", new Location());

        return "location";
    }

    @PostMapping(value = "/api/location")
    @ResponseBody
    public String handlePost(@RequestBody Location location, @AuthenticationPrincipal User user) {


        log.debug(location.toString());
        log.debug(user.toString());

        user.setLongitude(location.getLongitude());
        user.setLatitude(location.getLatitude());

        userRepository.save(user);

        return location.toString();
    }

}
