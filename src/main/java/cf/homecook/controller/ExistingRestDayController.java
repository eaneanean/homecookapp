package cf.homecook.controller;

import cf.homecook.dto.TimeOffForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.TimeOff;
import cf.homecook.repo.TimeOffRepository;
import cf.homecook.validator.TimeOffFormValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RequestMapping("cook/allTimeOff")
@Slf4j
@Controller
public class ExistingRestDayController {

    @Autowired
    TimeOffRepository timeOffRepository;

    @Autowired
    TimeOffFormValidator validator;


    @GetMapping
    public String handleGet(@AuthenticationPrincipal Cook cook, Model model) {

        List<TimeOff> timeOffs =  timeOffRepository.findTimeOffsByCookIdOrderByUntilTimeDesc(cook.getId());
        model.addAttribute("timeOffs", timeOffs);
        return "allTimeOffs";
    }

    @GetMapping("/{id}")
    public String handleGetWithId(@PathVariable("id") long timeOffId, Model model) {

        Optional<TimeOff> optionalTimeOff = timeOffRepository.findById(timeOffId);

        if(optionalTimeOff.isPresent()) {
            TimeOffForm timeOffForm = new TimeOffForm();
            TimeOff timeOff = optionalTimeOff.get();
            LocalDateTime fromTime = timeOff.getFromTime().toLocalDateTime();
            LocalDateTime untilTime = timeOff.getUntilTime().toLocalDateTime();
            timeOffForm.setFromAndUntil(fromTime, untilTime);
            model.addAttribute("timeOffForm", timeOffForm);
            return "offTimeChange";
        }
        else {
            model.addAttribute("message", "Time off with id " +
                    timeOffId + " does not exist");
            return "error";
        }

    }

    @PostMapping("/{id}")
    public String handlePost(TimeOffForm timeOffForm,
                             BindingResult result,
                             @AuthenticationPrincipal Cook cook,
                             Model model,
                             @PathVariable("id") long timeOffId)  {


        log.debug("cook/off POST method");
        log.debug(timeOffForm.toString());

        validator.validate(timeOffForm, result);

        if(result.hasErrors()) {
            log.debug("Result has errors");
            result.getAllErrors()
                    .stream()
                    .forEach(error -> log.debug(error.toString()));

            model.addAttribute("isThereError", true);
            return "offTimeChange";
        }

        log.debug("Result has no errors");

        TimeOff timeOff = new TimeOff();

        timeOffForm.toTimeOff(timeOff);
        timeOff.setCook(cook);
        timeOff.setId(timeOffId);

        timeOffRepository.save(timeOff);


        return "redirect:/cook/allTimeOff";
    }



}
