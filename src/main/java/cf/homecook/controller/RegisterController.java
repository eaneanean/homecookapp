package cf.homecook.controller;

import cf.homecook.dto.RegisterForm;
import cf.homecook.entity.Dish;
import cf.homecook.entity.Token;
import cf.homecook.entity.User;
import cf.homecook.repo.DishRepository;
import cf.homecook.repo.TokenRepository;
import cf.homecook.repo.UserRepository;
import cf.homecook.service.ConformationService;
import cf.homecook.service.EmailService;
import cf.homecook.service.TokenService;
import cf.homecook.service.UserService;
import cf.homecook.validator.RegisterFormValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Optional;

@Controller
@RequestMapping("/register")
@Slf4j
public class RegisterController {

    @Autowired
    RegisterFormValidator registerFormValidator;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    ConformationService conformationService;


    @GetMapping
    public String handleGet(Model model) {

        log.debug("handleGet() on /register");

        model.addAttribute(new RegisterForm());
        return "register";
    }

    @PostMapping
    public String handlePost(@Valid RegisterForm registerForm,
                             BindingResult result,
                             HttpServletRequest request,
                             Model model) {

        log.debug(registerForm.toString());

        registerFormValidator.validate(registerForm, result);

        if(result.hasErrors()) {
            result.getAllErrors()
                    .stream()
                    .forEach(error -> log.debug(error.toString()));
            return "register";
        }

        User savedUser = userRepository.save(userService.formToUser(registerForm));

        Token token = tokenService.createToken(savedUser);

        tokenRepository.save(token);


        try {
            conformationService.sendConformationMail(savedUser, token, conformationService.getURI(request, "/conformation"));
        } catch (MalformedURLException e) {
            log.error(e.toString());
        } catch (URISyntaxException e) {
            log.error(e.toString());
        }

        model.addAttribute("message", "Check your email to confirm your registration");
        return "success";
    }


}
