package cf.homecook.controller;

import cf.homecook.dto.DaysForm;
import cf.homecook.dto.WorkingDayDto;
import cf.homecook.entity.Cook;
import cf.homecook.entity.WorkingDay;
import cf.homecook.repo.DayRepository;
import cf.homecook.service.DayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/cook/day")
@Slf4j
public class DayController {

    @Autowired
    DayRepository dayRepository;

    @Autowired
    DayService dayService;

    private final int DAYS_IN_A_WEEK = 7;

    @GetMapping
    public String handeGet(@AuthenticationPrincipal Cook cook, Model model) {

        List<WorkingDay> workingDayList =  dayRepository.findByCookIdOrderByDayOfTheWeek(cook.getId());

        if(workingDayList.size() == 0) {

            for (int i = 0; i < DAYS_IN_A_WEEK; i++) {

                WorkingDay day = new WorkingDay();
                day.setCook(cook);
                day.setDayOfTheWeek(i);

                // default time. Nothing special about this time values
                // but it seems reasonable

                Time from = Time.valueOf("16:00:00");
                Time until = Time.valueOf("22:00:00");

                if(from == null)
                    log.debug("From is null");
                else
                    log.debug("From is not null" + from);

                if(until == null)
                    log.debug("Until is null");
                else
                    log.debug("Until is not null" + until);

                day.setFromTime(from);
                day.setUntilTime(until);
                day.setWorkDay(true);

                workingDayList.add(day);
            }

            dayRepository.saveAll(workingDayList);
        }

        workingDayList.stream()
                .forEach(day -> log.debug(day.toString()));

        List<WorkingDayDto> workingDayDtos = new ArrayList<>();

        for(int i = 0; i < DAYS_IN_A_WEEK; i++)
            workingDayDtos.add(i, new WorkingDayDto().fromWorkingDay(workingDayList.get(i)));

        workingDayDtos
                .stream()
                .forEach(dto -> log.debug(dto.toString()));

        DaysForm daysForm = new DaysForm();
        daysForm.setList(workingDayDtos);

        for(int i = 0; i < workingDayDtos.size(); i++) {
            WorkingDayDto dto = workingDayDtos.get(i);
            dto.setDayName(dayService.dayToDayName(dto.getDayOfTheWeek()));
        }

        model.addAttribute("daysForm", daysForm);

        return "addDay";
    }

    @GetMapping("/all")
    @ResponseBody
    public List<WorkingDay> listAll(@AuthenticationPrincipal Cook cook) {
        return dayRepository.findByCookIdOrderByDayOfTheWeek(cook.getId());
    }

    @PostMapping
    public String handleGet(DaysForm daysForm, @AuthenticationPrincipal Cook cook) {

        List<WorkingDayDto> workingDayDto = daysForm.getList();

        for(int i = 0; i < workingDayDto.size(); i++)
            workingDayDto.get(i).setCook(cook);

        workingDayDto
                .stream()
                .forEach(day -> log.debug(day.toString()));

        List<WorkingDay> workingDayList = new ArrayList<>();

        for(int i = 0; i < workingDayDto.size(); i++)
            workingDayList.add(i, workingDayDto.get(i).toWorkingDay(new WorkingDay()));


        dayRepository.deleteByCookId(cook.getId());
        dayRepository.saveAll(workingDayList);

        return "redirect:/cook/day";
    }


}
