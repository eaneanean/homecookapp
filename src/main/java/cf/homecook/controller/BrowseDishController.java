package cf.homecook.controller;

import cf.homecook.dto.ChangeDishForm;
import cf.homecook.entity.Cook;
import cf.homecook.entity.Dish;
import cf.homecook.repo.DishRepository;
import cf.homecook.service.DishService;
import cf.homecook.service.ImageService;
import cf.homecook.service.ImageServiceHelper;
import cf.homecook.validator.AddDishFormValidator;
import cf.homecook.validator.ImageValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import static cf.homecook.service.ImageServiceHelper.PREFIX_DISH;
import static cf.homecook.service.ImageServiceHelper.PREFIX_SMALL_PICTURES;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequestMapping("/cook/browse")
@Slf4j
@Controller
public class BrowseDishController {

    @Autowired
    DishRepository dishRepository;

    @Autowired
    DishService dishService;

    @Autowired
    ImageService imageService;

    @Autowired
    AddDishFormValidator addDishFormValidator;

    @Autowired
    ImageValidator imageValidator;


    @GetMapping
    public String handleGet(@AuthenticationPrincipal Cook cook, Model model) {
        log.debug("/cook/browse GET method");

        List<Dish> dishes =  dishRepository.findByCookId(cook.getId());

        for(int i = 0; i < dishes.size(); i++) {
            try {
                Dish currentDish = dishes.get(i);

                String imageName = PREFIX_SMALL_PICTURES
                        + PREFIX_DISH + currentDish.getId();

                log.debug("Image name:" + imageName);

                String image = imageService.readBase64OfImage(imageName);

                currentDish.setImageInBase64(image);
            }
            catch (IOException io) {
                log.debug("This shouldn't happen. Every dish must have a picture.");
                log.debug(io.toString());
            }
        }

        model.addAttribute("dishes", dishes);

        return "browseDish";
    }

    @GetMapping("/{id}")
    public String handleIndividualDish(@PathVariable("id") long id,
                                      @AuthenticationPrincipal Cook cook,
                                       Model model) {

        Optional<Dish> optionalDish = dishRepository.findById(id);

        if(optionalDish.isEmpty()) {
            model.addAttribute("message", "Dish with this id does not exist");
            return "error";
        }

        Dish dish = optionalDish.get();

        if(dish.getCook().getId() != cook.getId()) {
            model.addAttribute("message", "You don't have rights to see this dish");
            return "error";
        }

        ChangeDishForm changeDishForm = dishService.fromDish(new ChangeDishForm(), dish);

        try {
            String image = imageService.readBase64OfImage(ImageServiceHelper.PREFIX_SMALL_PICTURES +
                    ImageServiceHelper.PREFIX_DISH  +
                    Long.toString(id));
            changeDishForm.setImageInBase64(image);
        } catch (IOException e) {
            log.error("This must NOT happen. Every dish must have an image");
            log.error(e.toString());
        }


        model.addAttribute(changeDishForm);

        return "change";
    }

    @PostMapping("/{id}")
    public String handleIndivivualPost(@Valid ChangeDishForm changeDishForm,
                                       BindingResult result,
                                       @PathVariable("id") long id,
                                       @AuthenticationPrincipal Cook cook) {

        MultipartFile image = changeDishForm.getImageFile();

        // it's okay here image to be empty because it only means
        // that user hasn't entered a new image
        if(!image.isEmpty()) {
            log.debug("image is NOT empty");
            imageValidator.validate(image, result);
        }
        else
            log.debug("image is empty");

        addDishFormValidator.validate(changeDishForm, result);

        if(result.hasErrors()) {
            result
                    .getAllErrors()
                    .stream()
                    .forEach(objectError -> log.debug(objectError.toString()));
            return "change";
        }


        if(!image.isEmpty()) {
            String imageName =  PREFIX_DISH + Long.toString(id);

            log.debug("Image name:" + imageName);

            try {
                imageService.saveImage(image, imageName);
            } catch (IOException e) {
                log.error(e.toString());
            }
        }

        Dish savedDish = dishService.replaceDish(changeDishForm, cook, id);

        return "redirect:/cook/browse";
    }

}
