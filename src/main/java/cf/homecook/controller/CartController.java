package cf.homecook.controller;

import cf.homecook.dto.AddToCard;
import cf.homecook.dto.AddToCardForm;
import cf.homecook.entity.Dish;
import cf.homecook.repo.DishRepository;
import cf.homecook.service.CartService;
import cf.homecook.service.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cf.homecook.service.ImageServiceHelper.PREFIX_DISH;
import static cf.homecook.service.ImageServiceHelper.PREFIX_SMALL_PICTURES;

@Controller
@RequestMapping("/cart")
@Slf4j
public class CartController {

    @Autowired
    CartService cartService;

    @Autowired
    DishRepository dishRepository;

    @Autowired
    ImageService imageService;

    @GetMapping
    public String handleGet(HttpServletRequest request, Model model, HttpServletResponse response) {

        log.debug("/cart GET method");

        try {
            List<AddToCard> addToCardList = cartService.cookiesToAddToCard(request.getCookies());
            String errorMsg = cartService.checkQuantity(addToCardList, response);
            log.debug("Error message:" + errorMsg);
            if (errorMsg == null)
                model.addAttribute("isThereError", false);
            else {
                model.addAttribute("isThereError", true);
                log.debug("isThereError: true");
                log.debug("error message:" + errorMsg);
                model.addAttribute("errorMsg", errorMsg);
            }

            model.addAttribute("addToCardForm", new AddToCardForm(addToCardList));

            addToCardList.forEach(item -> {
                Optional<Dish> dishOptional = dishRepository.findById(item.getDishId());
                if (dishOptional.isPresent()) {
                    Dish dish = dishOptional.get();
                    item.setTitle(dish.getTitle());
                    item.setAvailableQuantity(dish.getQuantity());
                    item.setPrice(dish.getPriceInEuro());

                    try {
                        String image = imageService.readBase64OfImage(PREFIX_SMALL_PICTURES + PREFIX_DISH + dish.getId());
                        item.setImage(image);
                    } catch (IOException e) {
                        log.debug(e.toString());
                    }
                } else {
                    log.debug("Dish with that id is not found. This could happen " +
                            "if the cook deleted the dish since the user added it to the" +
                            " card or when the user tempered with the cookie");
                }
            });

        } catch (ArrayIndexOutOfBoundsException exception) {
            log.debug("This happens when there are no");
            model.addAttribute("addToCardForm", new AddToCardForm(new ArrayList<AddToCard>()));
        }


        return "addToCardForm";
    }


}
