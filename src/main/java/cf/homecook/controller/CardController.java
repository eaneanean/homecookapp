package cf.homecook.controller;

import cf.homecook.dto.AddToCard;
import cf.homecook.dto.PurchaseWithAddress;
import cf.homecook.dto.SelectAddressForm;
import cf.homecook.entity.*;
import cf.homecook.repo.AddressRepository;
import cf.homecook.repo.OrderDishRepository;
import cf.homecook.repo.PurchaseRepository;
import cf.homecook.service.CardService;
import cf.homecook.service.CartService;
import cf.homecook.service.EmailService;
import cf.homecook.service.PurchaseWithAddressService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.criteria.Order;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/card")
@Slf4j
public class CardController {

    @Autowired
    CartService cartService;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    CardService cardService;

    @Autowired
    OrderDishRepository orderDishRepository;

    @Autowired
    PurchaseWithAddressService purchaseWithAddressService;

    @Autowired
    EmailService emailService;

    @GetMapping
    public String handleGet(HttpServletRequest request, Model model,
                            @AuthenticationPrincipal User user, HttpServletResponse response) {

        List<AddToCard> addToCardList = cartService.cookiesToAddToCard(request.getCookies());
        String errorMsg = cartService.checkQuantity(addToCardList, response);

        // if there is an error go to /cart(checkQuantity returns null when there is no error)
        if(errorMsg != null) {
            return "redirect:/cart";
        }

        boolean atLeastOneDelivery = cartService.cookiesToAddToCard(request.getCookies())
                .stream()
                .anyMatch(addToCard -> addToCard.getDelivery());

        log.debug("atLeastOneDelivery:" + atLeastOneDelivery);

        if(atLeastOneDelivery) {
            List<Address> listOfAddressForThePrincipal;
            listOfAddressForThePrincipal = addressRepository.findAddressesByUserId(user.getId());
            model.addAttribute("addresses", listOfAddressForThePrincipal);
        }

        // if the user picks up the dish(es) himself/herself, then address is not
        // needed. But if there is at least one dish for delivery, then the user should
        // select the wanted address for that order.
        model.addAttribute("showAddress", atLeastOneDelivery);
        model.addAttribute("selectAddressForm", new SelectAddressForm());
        return "card";
    }

    @PostMapping
    public String handlePost(SelectAddressForm form, Model model, HttpServletRequest request,
                             @AuthenticationPrincipal User user, HttpServletResponse response) {
        log.debug("/card POST method");
        log.debug("form address:" + form.getAddressId());

        // addressId could be zero, if the user
        // hasn't got a single dish that wants to be deliver
        // on his door, so the form dosn't even show
        long addressId = form.getAddressId();


        // check whether someone bought dish while the user was entering
        // information about the credit card so that the asked quantity
        // is no longer available. If that was the case, redirect the user
        // to the cart
        List<AddToCard> addToCardList = cartService.cookiesToAddToCard(request.getCookies());
        String errorMsg = cartService.checkQuantity(addToCardList, response);
        if(errorMsg != null)
            return "redirect:/cart";

        Purchase purchase = new Purchase();

        boolean atLeastOneDelivery =
                addToCardList
                .stream()
                .anyMatch(addToCard -> addToCard.getDelivery());

        if(atLeastOneDelivery)
            log.debug("The purchase has at least one delivery dish");
        else
            log.debug("The purchase dosn't not have a single delivery dish");

        if(atLeastOneDelivery) {
            Optional<Address> optionalAddress = addressRepository.findById(form.getAddressId());

            if (optionalAddress.isEmpty())
                throw new RuntimeException("Can't really make an order with non existant address");

            Address address = optionalAddress.get();

            if(address.getUser().getId() != user.getId())
                throw new RuntimeException("Must be the same user that is logged in and " +
                        "that has used the address");

            purchase.setAddress(address);
        }

        purchase.setUser(user);
        purchase.setOrderMadeAt(System.currentTimeMillis());

        try {
            // now it has an id
            String s = cardService.makePayment(purchase, addToCardList).toString();
            emailService.sendSimpleMessage(user.getEmail(), "Order conformation",
                    "You have ordered the following items:\n" + s);


//            Map<Cook, List<OrderDish>> cookToListOfOrderDishes = new HashMap<>();
//
//            for(OrderDish orderDish : purchase.getItems()) {
//
//                Dish dish = orderDish.getDish();
//                Cook cook = dish.getCook();
//
//                log.debug("Cook from orderDish:" + cook);
//
//                if(cookToListOfOrderDishes.containsKey(cook)) {
//                    log.debug("This cook already exists.");
//                    List<OrderDish> orderDishes = cookToListOfOrderDishes.get(cook);
//                    orderDishes.add(orderDish);
//                    cookToListOfOrderDishes.put(cook, orderDishes);
//
//                }
//                else {
//                    log.debug("Cook is for the first time.");
//                    List<OrderDish> orderDishes = new ArrayList<>();
//                    orderDishes.add(orderDish);
//                    cookToListOfOrderDishes.put(cook, orderDishes);
//                }
//            }
//
//            log.debug("Map size:"+ cookToListOfOrderDishes.size());








            Map<Cook, List<OrderDish>> map = purchase.getItems().stream()
                    .collect(Collectors.groupingBy(orderDish -> orderDish.getDish().getCook()));


            map.forEach((cook, orderDishList) -> {

                String message = user.getFirstName() + " " + user.getLastName() + " has just " +
                        "ordered from you:\n";

                for(OrderDish orderDish : orderDishList) {
                    message += orderDish.getDish().getTitle() + " with quantity:" + orderDish.getQuantity();
                    message += " and time:" + orderDish.getChosenTime();
                    if(!orderDish.isDelivery())
                        message += " for delivery\n";
                    else
                        message += " and the user will pick it up.\n";


                }

                log.debug("Email content:");
                log.debug("Cook:" + cook + " " + message);

                emailService.sendSimpleMessage(cook.getEmail(), "You have a new order",
                        message);
            });



//add cookie to response
            log.debug("All the cookies");
            Arrays.stream(request.getCookies()).forEach(cookie ->  {
                log.debug(cookie.getName());
                log.debug("Secure:" + cookie.getSecure());
                log.debug("Path:" + cookie.getPath());
            });


            Cookie cookie = new Cookie("items", URLEncoder.encode("[]", "UTF-8"));
            cookie.setSecure(false);
            cookie.setPath(null);

            response.addCookie(cookie);
        }

        catch (RuntimeException exception) {
            log.error("The whole transaction was rolled back");
            log.error(exception.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        return "redirect:/browse";
    }


}
