package cf.homecook.controller;

import cf.homecook.dto.AddToCard;
import cf.homecook.entity.Purchase;
import cf.homecook.entity.User;
import cf.homecook.repo.PurchaseRepository;
import cf.homecook.service.CartService;
import cf.homecook.service.ConformationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Controller
@Slf4j
public class PurchaseController {

    @Autowired
    CartService cartService;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ConformationService conformationService;


    @GetMapping(value = {"/purchase/{page}", "/purchase"})
    public String handleGet(HttpServletRequest request, Model model,
                                    @PathVariable("page") Optional<Integer> page,
                                    @AuthenticationPrincipal User user) {

        int requestPage = 1;

        if(page.isPresent())
            requestPage = page.get();

        Page<Purchase> pagePurchase =  purchaseRepository.findByUserId(user.getId(),
                PageRequest.of(requestPage - 1, 5, Sort.Direction.DESC, "orderMadeAt"));

        model.addAttribute("purchases", pagePurchase );
        model.addAttribute("isTherePrevious", pagePurchase.hasPrevious());
        model.addAttribute("isThereNext", pagePurchase.hasNext());

        String url = null;
        try {
            String uri = conformationService.getURI(request, "");
            log.debug("uri:" + uri);
            url = uri + "/purchase/";
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        String previousPageUrl = url + String.valueOf(requestPage - 1);
        String nextPageUrl = url + String.valueOf(requestPage + 1);

        log.debug("previous page url:" + previousPageUrl);
        log.debug("next page url:" + nextPageUrl);

        model.addAttribute("previous", previousPageUrl);
        model.addAttribute("next", nextPageUrl);

        return "orders";
    }

}
