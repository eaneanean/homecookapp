package cf.homecook.repo;

import cf.homecook.entity.OrderDish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDishRepository extends JpaRepository<OrderDish, Long> {

    @Query(value = "SELECT COUNT(*) " +
            "FROM order_dish, purchase " +
            "WHERE order_dish.purchase_id  = purchase.id " +
            "AND user_id = :userId " +
            "AND dish_id = :dishId ", nativeQuery = true)
    public int numberOfTimesDishWasOrdered(@Param("dishId")long dishId,@Param("userId") long userId);


}
