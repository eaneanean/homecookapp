package cf.homecook.repo;

import cf.homecook.entity.WorkingDay;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface DayRepository extends CrudRepository<WorkingDay, Long> {

    List<WorkingDay> findByCookIdOrderByDayOfTheWeek(long cookId);
    long deleteByCookId(long cookId);
    
    
}
