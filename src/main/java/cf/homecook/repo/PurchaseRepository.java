package cf.homecook.repo;

import cf.homecook.entity.Purchase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PurchaseRepository extends PagingAndSortingRepository<Purchase, Long> {


    Page<Purchase> findByUserId(long userId, Pageable pageable);

}
