package cf.homecook.repo;

import cf.homecook.dto.DishDto;
import cf.homecook.utils.Sorting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class DishDtoRepositoryImp {

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public DishDtoRepositoryImp(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<DishDto> find(long userId, String title, String sorting, double distance,
                              String chosenDate, String chosenTime, String currentDateTime) {

        String sortSql = Sorting.requestParameterToSql(sorting);

        SqlParameterSource parameterSource = new MapSqlParameterSource("myId", userId)
                .addValue("distance", distance)
                .addValue("chosenDate", chosenDate)
                .addValue("chosenTime", chosenTime)
                .addValue("currentDateTime", currentDateTime)
                .addValue("title", "%" + title + "%");

        log.debug("Title:" + title);


        String sql = "SELECT * \n" +
                "FROM (\n" +
                "SELECT dish.id as dishId, dish.cook_id as cook_id, distance, time_to_prepare_in_minutes, title, user.first_name, user.last_name, price_in_euro as price \n" +
                "FROM (\n" +
                "SELECT id, distance\n" +
                "  FROM (\n" +
                " SELECT \n" +
                "        z.id,\n" +
                "        z.latitude, z.longitude,\n" +
                "        p.radius,\n" +
                "        p.distance_unit\n" +
                "                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))\n" +
                "                 * COS(RADIANS(z.latitude))\n" +
                "                 * COS(RADIANS(p.longpoint - z.longitude))\n" +
                "                 + SIN(RADIANS(p.latpoint))\n" +
                "                 * SIN(RADIANS(z.latitude)))) AS distance\n" +
                "  FROM user AS z\n" +
                "  JOIN (   /* these are the query parameters */\n" +
                "        SELECT  latitude  AS latpoint,  longitude AS longpoint,\n" +
                "                :distance AS radius,      111.045 AS distance_unit\n" +
                "                FROM user where id = :myId\n" +
                "    ) AS p ON 1=1\n" +
                "  WHERE z.latitude\n" +
                "     BETWEEN p.latpoint  - (p.radius / p.distance_unit)\n" +
                "         AND p.latpoint  + (p.radius / p.distance_unit)\n" +
                "    AND z.longitude\n" +
                "     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))\n" +
                "         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))\n" +
                " ) AS d\n" +
                " WHERE distance <= radius AND d.id <> :myId \n" +
                " ) AS withinDistance, working_day, dish, user\n" +
                " WHERE dish.cook_id  = user.id AND\n" +
                " dish.quantity > 0 AND\n" +
                " withinDistance.id = working_day.cook_id AND\n" +
                " day_of_the_week = WEEKDAY(:chosenDate)\n" +
                " AND is_work_day = 1  \n" +
                " AND :chosenTime BETWEEN working_day.from_time AND working_day.until_time\n" +
                "  AND withinDistance.id NOT IN (SELECT cook_id\n" +
                "FROM time_off\n" +
                "WHERE CONCAT(:chosenDate, \" \", :chosenTime) BETWEEN from_time AND until_time)\n" +
                "AND dish.cook_id  = withinDistance.id AND\n" +
                "TIMEDIFF(CONCAT(:chosenDate, \" \", :chosenTime), :currentDateTime) > SEC_TO_TIME(dish.time_to_prepare_in_minutes * 60)\n" +
                ") AS rezultat LEFT JOIN dish_rating_view \n" +
                "ON rezultat.dishId = dish_rating_view.dish_id\n" +
                "WHERE title LIKE :title OR first_name LIKE :title OR last_name LIKE :title OR CONCAT_WS(' ', first_name , last_name) LIKE :title\n" +
                "ORDER BY " + sortSql + ";\n";


        return jdbcTemplate.query(sql, parameterSource, (ResultSet rs) -> {
            List<DishDto> dishDtos = new ArrayList<>();

            while(rs.next()) {
                DishDto dishDto = new DishDto(
                    rs.getLong("dishId"),
                    rs.getLong("cook_id"),
                    rs.getDouble("distance"),
                    rs.getInt("time_to_prepare_in_minutes"),
                    rs.getString("title"),
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getDouble("average"),
                    rs.getInt("total"),
                    rs.getDouble("price")
                );

                dishDtos.add(dishDto);
            }


            return dishDtos;
        });
    }


}
