package cf.homecook.repo;

import cf.homecook.entity.Dish;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DishRepository extends CrudRepository<Dish, Long> {

    public List<Dish> findByCookId(long cookId);
}
