package cf.homecook.repo;

import cf.homecook.dto.CookOrder;
import cf.homecook.dto.DishDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class CookOrderRepositoryImp implements CookOrderRepository {


    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    public List<CookOrder> findAllCookOrders(long cookId) {
        SqlParameterSource parameterSource = new MapSqlParameterSource("cookId", cookId);

        String sql = "SELECT od.chosen_time as time, od.is_delivery as delivery, od.quantity as quantity , d.title as title, u.first_name as fname, u.last_name as lname \n" +
                "FROM order_dish od, dish d, cook c, purchase p, `user` u\n" +
                "WHERE od.dish_id = d.id \n" +
                "AND d.cook_id = c.id \n" +
                "AND c.id = :cookId\n" +
                "AND od.purchase_id = p.id \n" +
                "AND p.user_id = u.id \n" +
                "ORDER BY od.chosen_time DESC\n";

        log.debug("Sql:" + sql);

        return jdbcTemplate.query(sql, parameterSource, (ResultSet rs) -> {
            List<CookOrder> dishDtos = new ArrayList<>();

            while(rs.next()) {
                CookOrder cookOrder = new CookOrder(
                        rs.getString("fname"),
                        rs.getString("lname"),
                        rs.getTimestamp("time"),
                        rs.getBoolean("delivery"),
                        rs.getString("title"),
                        rs.getInt("quantity")
                );

                dishDtos.add(cookOrder);
            }


            return dishDtos;
        });
    }
}
