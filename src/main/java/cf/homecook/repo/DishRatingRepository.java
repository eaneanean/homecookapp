package cf.homecook.repo;

import cf.homecook.entity.Client;
import cf.homecook.entity.DishRating;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DishRatingRepository extends CrudRepository<DishRating, Long> {


    Optional<DishRating> findByClientIdAndDishId(long clientId, long dishId);

}
