package cf.homecook.repo;

import cf.homecook.dto.CookOrder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CookOrderRepository {
    public List<CookOrder> findAllCookOrders(long cookId);
}
