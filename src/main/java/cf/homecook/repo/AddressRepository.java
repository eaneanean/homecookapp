package cf.homecook.repo;

import cf.homecook.entity.Address;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AddressRepository extends CrudRepository <Address, Long> {

    public List<Address> findAddressesByUserId(long userId);
    public Optional<Address> findAddressByUserId(long userId);
}
