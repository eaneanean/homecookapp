package cf.homecook.repo;

import cf.homecook.entity.Token;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<Token, Long> {

    public Optional<Token> findByRandomString(String randomString);

}
