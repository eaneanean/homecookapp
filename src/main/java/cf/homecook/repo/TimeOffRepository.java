package cf.homecook.repo;

import cf.homecook.entity.TimeOff;
import org.springframework.data.repository.CrudRepository;

import java.sql.Time;
import java.util.List;

public interface TimeOffRepository extends CrudRepository <TimeOff, Long> {

    List<TimeOff> findTimeOffsByCookIdOrderByUntilTimeDesc(long cookId);

}
