package cf.homecook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(generator = "address_seq")
    long id;

    String address1;
    String address2;

    String city;
    String country;

    String postcode;

    @ManyToOne
    User user;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(address1 + " ");
        if(address2 != null)
            sb.append(address2 + " ");

        sb.append(city);
        sb.append("\n");

        return sb.toString();
    }
}
