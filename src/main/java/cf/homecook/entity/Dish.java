package cf.homecook.entity;

import cf.homecook.dto.AddDishForm;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Dish {

    @Id
    @GeneratedValue(generator = "dish_seq")
    long id;

    String title;

    @Column(columnDefinition = "TEXT", length = 2000)
    String description;

    double priceInEuro;

    int timeToPrepareInMinutes;

    @ManyToOne
    Cook cook;

    @OneToMany(mappedBy = "dish")
    private List<DishRating> ratings = new ArrayList<DishRating>();

    @Transient
    String imageInBase64;

    int quantity;

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priceInEuro=" + priceInEuro +
                ", timeToPrepareInMinutes=" + timeToPrepareInMinutes +
                ", cookId=" + cook.getId() +
                ", name=" + cook.getFirstName() +
                '}';
    }


}
