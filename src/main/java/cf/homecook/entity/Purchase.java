package cf.homecook.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Purchase {

    @Id
    @GeneratedValue(generator = "order_seq")
    private long id;

    @OneToMany(mappedBy = "purchase")
    private List<OrderDish> items = new ArrayList<>();

    @ManyToOne
    private User user;

    @ManyToOne
    private Address address;

    private long orderMadeAt;
    private double totalInEuros;


    @Override
    public String toString() {
        return "order: " + id;
    }


}
