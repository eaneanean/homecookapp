package cf.homecook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"dish_id", "client_id"})
})
public class DishRating {

    @Id
    @GeneratedValue(generator = "dish_rating_seq")
    long id;

    @ManyToOne
    Dish dish;

    @ManyToOne
    Client client;

    int stars;

    //in UTC milliseconds (UTC 0)
    private long createdAt;

    @Override
    public String toString() {
        return "stars: " + stars;
    }
}
