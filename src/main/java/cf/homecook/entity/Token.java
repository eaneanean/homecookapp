package cf.homecook.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.Instant;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Token {

    @Id
    @GeneratedValue(generator = "token_seq")
    private long id;

    @ManyToOne
    private User user;

    private long createdAt;

    // TODO: make this column index
    private String randomString;

    /**
     *
     * @param expirationTimeInMilliseconds in milliseconds. Must be bigger than or equal to 0.
     * @return whether the token is expired at the moment of the function call.
     */
    public boolean isTokenExpired(long expirationTimeInMilliseconds) {
        if(expirationTimeInMilliseconds < 0)
            throw new IllegalArgumentException(("Exception time must be bigger than 0"));
        return (createdAt + expirationTimeInMilliseconds) < Instant.now().toEpochMilli();
    }


}
