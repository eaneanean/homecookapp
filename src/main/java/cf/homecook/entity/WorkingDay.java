package cf.homecook.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Time;

@Entity
@Data
@NoArgsConstructor
public class WorkingDay {

    @Id
    @GeneratedValue(generator = "day_seq")
    private long id;


    /**
     *  0 - Monday
     *  1 - Tuesday
     *  2 - Wednesday
     *  3- Thursday
     *  4 - Friday
     *  5 - Saturday
     *  6 - Sunday
     */
    private int dayOfTheWeek;

    @ManyToOne
    private Cook cook;

    private Time fromTime;

    private Time untilTime;

    // this is boolean instead of using the absence
    // of values in the fromTime and untilTime column
    // because we want to able not to lose information when
    // the user often flips whether it is a working day
    private boolean isWorkDay;




}
