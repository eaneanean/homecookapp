package cf.homecook.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
public class TimeOff {

    @Id
    @GeneratedValue(generator = "time_seq")
    long id;

    @ManyToOne
    Cook cook;

    Timestamp fromTime;

    Timestamp untilTime;

}
