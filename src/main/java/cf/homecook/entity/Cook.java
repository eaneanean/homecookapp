package cf.homecook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Cook extends User {

    @NonNull
    @NotNull
    @Size(min = 50, max = 2000)
    private String description;

    @Override
    public String toString() {
        return "Cook: " + super.toString();
    }

    @Override
    public boolean equals(Object cook) {

        if(cook == null || !(cook instanceof Cook))
            return false;

        return this.getId() == ((Cook) cook).getId();
    }

}
