package cf.homecook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Data
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(indexes = {
        @Index(columnList = "latitude"),
        @Index(columnList = "longitude")
})
public class User implements UserDetails {

    // user could be either client or cook
    // both are users
    public static final String ROLE_CLIENT = "ROLE_CLIENT";
    public static final String ROLE_COOK = "ROLE_COOK";

    @Id
    @GeneratedValue(generator = "user_seq")
    private long id;

    //in UTC milliseconds (UTC 0)
    private long createdAt;

    private String firstName;

    private String lastName;

    private String email;

    private double latitude;

    private double longitude;

    private String encryptedPassword;

    private boolean enabled;

    // could be either ROLE_CLIENT, or ROLE_COOK.
    private String role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(role.equals(ROLE_CLIENT))
            return Collections.singleton(new SimpleGrantedAuthority(ROLE_CLIENT));
        else if(role.equals(ROLE_COOK))
            return Collections.singleton(new SimpleGrantedAuthority(ROLE_COOK));
        else
            throw new RuntimeException("This must happen. User is " +
                    "either client or a cook ");
    }

    @Override
    public String getPassword() {
        return encryptedPassword;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {

        return enabled;
    }
}
