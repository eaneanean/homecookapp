package cf.homecook.entity;

@javax.persistence.SequenceGenerator(name ="user_seq")
@javax.persistence.SequenceGenerator(name ="dish_seq")
@javax.persistence.SequenceGenerator(name ="dish_rating_seq")
@javax.persistence.SequenceGenerator(name ="token_seq")
@javax.persistence.SequenceGenerator(name ="day_seq")
@javax.persistence.SequenceGenerator(name ="time_seq")
@javax.persistence.SequenceGenerator(name ="address_seq")
@javax.persistence.SequenceGenerator(name ="order_seq")
@javax.persistence.SequenceGenerator(name ="order_dish_seq")
public class SequenceGenerator {

    // empty class used only so that the all sequence generators
    // will be at the same place
}
