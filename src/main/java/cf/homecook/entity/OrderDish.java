package cf.homecook.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor

/**
 * Breaks the 2 normal form, because price is functionally dependent
 * on dish, and (dish, order) make candidate key.
 * If price is not saved, when the user changes the price, the further calculations
 * will not be correct, because the new price of the dish will be used.
 *
 * Other solution is to make another table when there is a start and an end date
 * for which the price was correct. This will normalize the table, but will add
 * complexity to the solution.
 *
 * Although the table is not normalized, the nature of the use case is such that
 * the anomalies usually associated with denormalized table won't exist, but it
 * it is a record about a transaction, that won't be later changed.
 */
public class OrderDish {

    @Id
    @GeneratedValue(generator = "order_dish_seq")
    private long id;

    @ManyToOne
    private Dish dish;

    @ManyToOne
    private Purchase purchase;

    private int quantity;
    private double pricePerDishInEuros;
    private boolean isDelivery;

    // chosen time either for delivery or for
    // picking up
    private Timestamp chosenTime;


    @Override
    public String toString() {
        return "OrderDish{" +
                "id=" + id +
                ", dish=" + dish +
                ", purchase=" + purchase +
                ", quantity=" + quantity +
                ", pricePerDishInEuros=" + pricePerDishInEuros +
                ", isDelivery=" + isDelivery +
                ", chosenTime=" + chosenTime +
                "Cook:" + dish.getCook() +
                '}';
    }
}
