package cf.homecook.utils;

public class Common {

    /**
     *
     * @return if value lesser than 10, adds zero. 7 becomes 07
     */
    public static String addZero(int value) {
        if(value < 10)
            return "0" + String.valueOf(value);
        else
            return String.valueOf(value);
    }

    /**
     *
     * @return if value lesser than 10, adds zero. 7 becomes 07
     */
    public static String addZero(String value) {
        return addZero(Integer.parseInt(value));
    }

}
