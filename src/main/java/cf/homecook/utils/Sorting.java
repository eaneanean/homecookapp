package cf.homecook.utils;

import java.util.HashMap;
import java.util.Map;

public class Sorting {

    private static final String TITLE_ASCENDING = "title ASC";
    private static final String TITLE_DESCENDING = "title DESC;";

    private static final String STAR_ASCENDING = "average ASC";
    private static final String STAR_DESCENDING = "average DESC";

    private static final String DISTANCE_CLOSER_FIRST = "distance ASC";
    private static final String DISTANCE_CLOSER_LAST = "distance DESC";

    private static final String PRICE_LOWER_FIRST = "price ASC";
    private static final String PRICE_HIGHER_FIRST = "price DESC";

    private static final String TIME_SHORT_FIRST = "time_to_prepare_in_minutes ASC";
    private static final String TIME_LONG_FIRST = "time_to_prepare_in_minutes DESC";


    public static String requestParameterToSql(String param) {

        Map<String, String> map = new HashMap<>();
        map.put("dist_asc", DISTANCE_CLOSER_FIRST);
        map.put("dist_desc", DISTANCE_CLOSER_LAST);
        map.put("star_asc", STAR_ASCENDING);
        map.put("star_desc", STAR_DESCENDING);
        map.put("title_asc", TITLE_ASCENDING);
        map.put("title_desc", TITLE_DESCENDING);
        map.put("price_asc", PRICE_LOWER_FIRST);
        map.put("price_desc", PRICE_HIGHER_FIRST);
        map.put("time_asc", TIME_SHORT_FIRST);
        map.put("time_desc", TIME_LONG_FIRST);

        return map.get(param);
    }

}

