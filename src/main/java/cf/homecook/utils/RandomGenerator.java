package cf.homecook.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;


public class RandomGenerator {

    private static final String alphaNumeric
            = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static String generateRandomBytes(int numberOfCharacters) throws Exception {
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder stringBuilder = new StringBuilder(numberOfCharacters);
        for(int i = 0; i < numberOfCharacters; i++)
            stringBuilder.append(alphaNumeric.charAt(secureRandom.nextInt(alphaNumeric.length())));
        return stringBuilder.toString();
    }


}